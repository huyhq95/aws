import TemplateForm from "../../client/components/templateForm";
import withI18n from "../../client/components/withI18n";
function AddTemplatePage({ i18n }) {
  return <TemplateForm i18n={i18n} />;
}

export default withI18n(AddTemplatePage);
