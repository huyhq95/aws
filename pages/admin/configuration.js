import NavigationTabs from "../../client/components/NavigationTabs";
import { Layout, Page, TextField, Card, Button } from "@shopify/polaris";
import withI18n from "../../client/components/withI18n";
import React, { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";

const ConfigurationPage = ({ i18n }) => {
  const [copied, setCopied] = useState(false);
  return (
    <React.Fragment>
      <NavigationTabs />
      <Card>
        <Page fullWidth>
          <Layout>
            <Layout.AnnotatedSection
              title={i18n.translate("Configuration.Layout.title")}
              description={i18n.translate("Configuration.Layout.description")}
            >
              <TextField
                value={CODE_PUT_THEME}
                multiline={10}
                disabled={true}
              />
              <CopyToClipboard
                text={CODE_PUT_THEME}
                onCopy={() => setCopied(true)}
              >
                <Button plain>
                  {copied
                    ? i18n.translate("Button.copied")
                    : i18n.translate("Button.copy")}
                </Button>
              </CopyToClipboard>
            </Layout.AnnotatedSection>
          </Layout>
        </Page>
      </Card>
    </React.Fragment>
  );
};

export default withI18n(ConfigurationPage);
