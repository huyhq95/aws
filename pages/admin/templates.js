import { useState, useEffect, useCallback } from "react";
import {
  Frame,
  Page,
  Card,
  DataTable,
  Link,
  Checkbox,
  Button,
  Toast,
} from "@shopify/polaris";
import { useRouter } from "next/router";
import { useQuery, useMutation } from "@apollo/react-hooks";
import withI18n from "../../client/components/withI18n";
import NavigationTabs from "../../client/components/NavigationTabs";
import { gql } from "apollo-boost";
import moment from "moment-timezone";
import { getTimeLastModifiled } from "../../client/utils/utility";
import SubscriptionPlan from "../../client/components/SubscriptionPlan";

const GET_TEMPLATES_AND_SHOP_INFO_QUERY = gql`
  query {
    getTemplatesByShop {
      message
      success
      data {
        _id
        templateName
        templateCode
        isDefault
        lastModifiled
      }
    }

    shopInfo
  }
`;

const UPDATE_TEMPLATE_DEFAULT = gql`
  mutation updateTemplateDefault($templateId: String, $lastModifiled: String) {
    updateTemplateDefault(
      templateId: $templateId
      lastModifiled: $lastModifiled
    ) {
      message
      success
    }
  }
`;

const TemplatesPage = ({ i18n }) => {
  const router = useRouter();
  const { data, refetch } = useQuery(GET_TEMPLATES_AND_SHOP_INFO_QUERY);

  const [
    updateTemplateDefault,
    { error: updateTemplateDefaultError },
  ] = useMutation(UPDATE_TEMPLATE_DEFAULT);
  const [templateId, setTemplateId] = useState("");

  const templates =
    data && data.getTemplatesByShop && data.getTemplatesByShop.data
      ? data.getTemplatesByShop.data
      : [];
  const timezone = data && data.shopInfo && data.shopInfo.timezone;

  const [currentTimestamp, setCurrentTimestamp] = useState(
    moment.tz(new Date(), timezone).valueOf()
  );
  const [activeToast, setActiveToast] = useState(false);

  useEffect(() => {
    (async () => {
      if (templateId) {
        await updateTemplateDefault({
          variables: {
            templateId: templateId,
            lastModifiled: currentTimestamp.toString(),
          },
          fetchPolicy: "no-cache",
        });
        setActiveToast(true);
      }
      await refetch();
    })();
  }, [templateId, router.route]);

  const toggleActiveToast = useCallback(
    () => setActiveToast((active) => !active),
    []
  );

  useEffect(() => {
    setInterval(() => {
      setCurrentTimestamp(moment.tz(new Date(), timezone).valueOf());
    }, 1000);
  }, []);

  const rowsTemplates =
    templates && templates.length > 0
      ? templates.map((template) => {
          let lastModifiled = getTimeLastModifiled(
            currentTimestamp,
            template.lastModifiled,
            timezone,
            i18n
          );
          return [
            <Link
              onClick={() => {
                router.push({
                  pathname: "/admin/edit-template",
                  query: { templateId: template._id },
                });
              }}
            >
              {template.templateName}
            </Link>,
            <Checkbox
              checked={
                templateId
                  ? templateId === template._id
                    ? true
                    : false
                  : template.isDefault
              }
              onChange={() => {
                setTemplateId(template._id);
              }}
            />,
            lastModifiled,
            <Button>
              {i18n.translate("Template.Button.duplicateTemplate")}
            </Button>,
            <Button destructive disabled={template.isDefault}>
              {i18n.translate("Template.Button.delete")}
            </Button>,
          ];
        })
      : [];

  const toastNotifications = activeToast ? (
    <Toast
      content={
        updateTemplateDefaultError
          ? i18n.translate("Template.Error.updateTemplateDefaultFail")
          : i18n.translate("Template.Success.successfullyUpdateTemplateDefault")
      }
      duration={2000}
      error={updateTemplateDefaultError ? true : false}
      onDismiss={toggleActiveToast}
    />
  ) : null;

  return (
    <Frame>
      <SubscriptionPlan i18n={i18n} />
      <NavigationTabs />
      <Page
        title={i18n.translate("Template.Page.title")}
        fullWidth
        primaryAction={{
          content: i18n.translate("Template.Page.primaryAction.content"),
          onAction: () => {
            router.push("/admin/add-template");
          },
        }}
      >
        <Card>
          <DataTable
            columnContentTypes={["text", "text", "text", "text", "numeric"]}
            headings={[
              i18n.translate("Template.DataTable.headings.template"),
              i18n.translate("Template.DataTable.headings.printByDefault"),
              i18n.translate("Template.DataTable.headings.lastModifiled"),
              "",
              "",
            ]}
            rows={rowsTemplates}
          />
        </Card>
        {toastNotifications}
      </Page>
    </Frame>
  );
};

export default withI18n(TemplatesPage);
