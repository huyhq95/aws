import { ApolloProvider } from "@apollo/react-hooks";
import App from "next/app";
import { AppProvider } from "@shopify/polaris";
import { Provider } from "@shopify/app-bridge-react";
import Cookies from "js-cookie";
import "@shopify/polaris/dist/styles.css";
import enTranslations from "@shopify/polaris/locales/en.json";
import jaTranslations from "@shopify/polaris/locales/ja.json";
import { I18nContext, I18nManager } from "@shopify/react-i18n";
import initApollo from "../client/utils/init-apollo";
import "../client/styles/app.css";

const apolloClient = initApollo({});

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    const shopOrigin = Cookies.get("shopOrigin");
    const locale = Cookies.get("locale") ? Cookies.get("locale") : "en-US";
    const translation =
      locale.indexOf("ja") == 0 ? jaTranslations : enTranslations;
    const i18nManager = new I18nManager({
      locale,
      onError(error) {
        console.log(error);
      },
    });
    console.log('AA1')
    return (
      <I18nContext.Provider value={i18nManager}>
        <AppProvider i18n={translation}>
          <Provider
            config={{
              apiKey: API_KEY,
              shopOrigin: shopOrigin,
              forceRedirect: true,
            }}
          >
            <ApolloProvider client={apolloClient}>
              <Component {...pageProps} />
            </ApolloProvider>
          </Provider>
        </AppProvider>
      </I18nContext.Provider>
    );
  }
}

export default MyApp;
