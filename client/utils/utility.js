import moment from "moment-timezone";

function getTimeLastModifiled(currentTimestamp, lastModifiled, timezone, i18n) {
  let response = "";
  if (lastModifiled) {
    let lastModifiledFomat = moment(
      moment.tz(parseInt(lastModifiled), timezone).format("YYYY-MM-DD HH:mm:ss")
    );
    let current = moment(
      moment(
        moment
          .tz(parseInt(currentTimestamp), timezone)
          .format("YYYY-MM-DD HH:mm:ss")
      )
    );
    let d = current.diff(lastModifiledFomat, "days");
    let h = current.diff(lastModifiledFomat, "hours");
    let m = current.diff(lastModifiledFomat, "minutes");
    let s = current.diff(lastModifiledFomat, "seconds");

    if (d) {
      response = moment
        .tz(parseInt(lastModifiled), timezone)
        .format("YYYY-MM-DD HH:mm");
    } else if (h) {
      response = i18n.translate(
        "Template.DataTable.content.lastModifiled.hours",
        { time: h }
      );
    } else if (m) {
      response = i18n.translate(
        "Template.DataTable.content.lastModifiled.minutes",
        { time: m }
      );
    } else if (s) {
      response = i18n.translate(
        "Template.DataTable.content.lastModifiled.seconds",
        { time: s }
      );
    }
  }

  return response;
}

module.exports = {
  getTimeLastModifiled,
};
