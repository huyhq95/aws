import { ApolloLink } from "apollo-link";
import { ApolloClient, InMemoryCache, HttpLink } from "apollo-boost";
import fetch from "isomorphic-fetch";

let apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch;
}

const shopifyApiLink = new HttpLink({
  uri: "/shopify/graphql",
});

const adminApiLink = new HttpLink({
  uri: "/admin/graphql",
});

function create(initialState) {
  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    link: ApolloLink.split(
      (operation) => operation.getContext().clientName === "shopify", // Shopify GraphQL API
      shopifyApiLink,
      adminApiLink // Admin GraphQL API
    ),
    cache: new InMemoryCache().restore(initialState || {}),
    fetchOptions: {
      credentials: "include",
    },
  });
}

export default function initApollo(initialState) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
