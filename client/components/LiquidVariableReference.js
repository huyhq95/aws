import { Modal, Layout, DataTable } from "@shopify/polaris";
import variableEn from "../../liquid/variable/en.json";
import variableJa from "../../liquid/variable/ja.json";
import Cookies from "js-cookie";

const LiquidVariableReference = ({ viewLiquid, handleViewLiquid, i18n }) => {
  const locale = Cookies.get("locale") ? Cookies.get("locale") : "en-US";
  const variable = locale.indexOf("ja") >= 0 ? variableJa : variableEn;
  const liquidVariables = Object.keys(variable).map((key) => [
    key,
    variable[key].description ? variable[key].description : "",
    variable[key].variable,
  ]);

  return (
    <div style={{ height: "538px" }}>
      <Modal
        open={viewLiquid}
        onClose={handleViewLiquid}
        title={i18n.translate("Template.Modal.title.variableLiquid")}
        secondaryActions={[
          {
            content: i18n.translate("Template.Button.close"),
            onAction: handleViewLiquid,
          },
        ]}
      >
        <Modal.Section>
          <Layout>
            {liquidVariables.map((liquidVariable, index) => {
              let rows = liquidVariable[2].map((row) => [
                row.description,
                row.name,
              ]);
              return (
                <Layout.AnnotatedSection
                  title={liquidVariable[0]}
                  description={liquidVariable[1]}
                  key={index}
                >
                  <DataTable
                    columnContentTypes={["text", "text"]}
                    headings={[
                      i18n.translate(
                        "Template.DataTable.headings.variableDescription"
                      ),
                      i18n.translate(
                        "Template.DataTable.headings.variableName"
                      ),
                    ]}
                    rows={rows}
                  />
                </Layout.AnnotatedSection>
              );
            })}
          </Layout>
        </Modal.Section>
      </Modal>
      <style jsx global>{`
        .Polaris-Modal-Dialog__Modal {
          max-width: 63%;
          max-height: 538px;
        }
        .Polaris-Modal__BodyWrapper {
          background-color: #f4f6f8;
        }
      `}</style>
    </div>
  );
};

export default LiquidVariableReference;
