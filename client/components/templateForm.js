import React from "react";
import { useState, useCallback, useEffect } from "react";
import {
  Page,
  Layout,
  Card,
  FormLayout,
  TextField,
  Stack,
  Label,
  Frame,
  Button,
  Checkbox,
  Toast,
  Spinner,
  SkeletonPage,
  SkeletonBodyText,
} from "@shopify/polaris";
import { gql } from "apollo-boost";
import { useMutation, useApolloClient } from "@apollo/react-hooks";
import { isEmpty } from "lodash";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import LiquidVariableReference from "./LiquidVariableReference";
import PreviewTemplateDialog from "./previewTemplateDialog";

const TextEditor = dynamic(import("./textEditor"), {
  ssr: false,
});

const SAVE_TEMPLATE_MUTATION = gql`
  mutation saveTemplate($template: templateInput, $templateId: String) {
    saveTemplate(template: $template, templateId: $templateId) {
      success
      message
    }
  }
`;

const GET_TEMPLATE_QUERY = gql`
  query getTemplate($templateId: String) {
    getTemplate(templateId: $templateId) {
      message
      success
      data {
        templateName
        templateCode
        isDefault
      }
    }
  }
`;

function TemplateForm(props) {
  const { i18n } = props;
  const router = useRouter();
  const { templateId } = router.query;
  const isEdit = router.pathname == "/admin/edit-template";
  const [activeToast, setActiveToast] = useState(false);
  const [typeToast, setTypeToast] = useState(true);
  const [msgToast, setMsgToast] = useState("");
  const [viewLiquid, setViewLiquid] = useState(false);
  const [activePreview, setActivePreview] = useState(false);
  const toggleActiveToast = useCallback(
    () => setActiveToast((active) => !active),
    []
  );

  const client = useApolloClient();
  const [isDefault, setDefault] = useState(false);
  const handleChangeDefault = useCallback(
    (newChecked) => setDefault(newChecked),
    []
  );
  const [templateName, setTemplateName] = useState("");
  const [templateCode, setTemplateCode] = useState("");
  const [loading, setLoading] = useState(true);
  const handleChangeTemplateName = useCallback(
    (newValue) => setTemplateName(newValue),
    []
  );
  const handleChangeTemplateCode = useCallback(
    (newValue) => setTemplateCode(newValue),
    []
  );

  const handleViewLiquid = useCallback(() => {
    setViewLiquid(!viewLiquid);
  }, [viewLiquid]);

  function handleActiveDialog() {
    setActivePreview(!activePreview);
  }

  const [saveTemplate, { loading: saveTemplateLoading }] = useMutation(
    SAVE_TEMPLATE_MUTATION,
    {
      onCompleted(data) {
        if (!data.saveTemplate.success) {
          setTypeToast(true);
          setActiveToast(true);
          setMsgToast(i18n.translate(data.saveTemplate.message));
        } else {
          if (isEdit) {
            setTypeToast(false);
            setActiveToast(true);
            setMsgToast(
              i18n.translate("Template.Success.successfullUpdateTemplate", {
                templateName: templateName,
              })
            );
          } else {
            router.push({
              pathname: "/admin/templates",
            });
          }
        }
      },
    }
  );

  useEffect(() => {
    (async () => {
      if (templateId) {
        let respone = await client.query({
          query: GET_TEMPLATE_QUERY,
          variables: {
            templateId: templateId,
          },
        });

        if (
          respone &&
          respone.data &&
          respone.data.getTemplate &&
          respone.data.getTemplate.data
        ) {
          const templateInfo = respone.data.getTemplate.data;
          setLoading(false);
          setDefault(templateInfo.isDefault);
          setTemplateName(templateInfo.templateName);
          setTemplateCode(templateInfo.templateCode);
        }
        if (client) {
          await client.stop();
          await client.resetStore();
        }
      }
    })();
  }, [templateId]);

  const errorNotifications = activeToast ? (
    <Toast content={msgToast} onDismiss={toggleActiveToast} error={typeToast} />
  ) : null;

  const isValidateForm = () => {
    if (
      isEmpty(templateName.trim()) ||
      templateName.trim().length < parseInt(MIN_TEMPLATE_NAME)
    ) {
      setTypeToast(true);
      setActiveToast(true);
      setMsgToast(
        i18n.translate("Template.Error.nameIsTooShort", {
          num: MIN_TEMPLATE_NAME,
        })
      );

      return false;
    }

    if (templateName.trim().length > parseInt(MAX_TEMPLATE_NAME)) {
      setTypeToast(true);
      setActiveToast(true);
      setMsgToast(
        i18n.translate("Template.Error.nameIsTooLong", {
          num: MAX_TEMPLATE_NAME,
        })
      );
      return false;
    }

    if (isEmpty(templateCode.trim())) {
      setTypeToast(true);
      setActiveToast(true);
      setMsgToast(i18n.translate("Template.Error.code"));
      return false;
    }
    return true;
  };

  async function submit() {
    const isValidate = await isValidateForm();
    if (isValidate) {
      await saveTemplate({
        variables: {
          template: {
            templateName: templateName.trim(),
            templateCode: templateCode.trim(),
            isDefault: isDefault,
          },
          templateId: templateId,
        },
      });
    }
  }

  if (isEdit && loading) {
    return (
      <SkeletonPage fullWidth>
        <Card.Section>
          <SkeletonBodyText />
        </Card.Section>
      </SkeletonPage>
    );
  }

  return (
    <Frame>
      <div className="headerButton">
        <Stack>
          <Stack.Item fill>
            <div className="button">
              <Button onClick={() => router.push("/admin/templates")}>
                {i18n.translate("Button.cancel")}
              </Button>
            </div>
            {isEdit && (
              <Button onClick={() => setActivePreview(true)}>
                {i18n.translate("Button.preview")}
              </Button>
            )}
          </Stack.Item>
          <Stack.Item>
            <Button primary onClick={!saveTemplateLoading ? submit : null}>
              {saveTemplateLoading ? (
                <Spinner size="small" color="teal" />
              ) : (
                i18n.translate("Button.save")
              )}
            </Button>
          </Stack.Item>
        </Stack>
      </div>
      <div className="template">
        <Page>
          <Layout>
            <Layout.AnnotatedSection
              title={i18n.translate("Template.Label.templateDetails")}
              description={
                <div>
                  <p>
                    {i18n.translate("Template.Label.customizeYourTemplate")}
                  </p>
                  <br />
                  <p>{i18n.translate("Template.Label.insertInformation")}</p>
                  <br />
                  <Button plain onClick={() => setViewLiquid(true)}>
                    {i18n.translate("Template.Label.viewLiquid")}
                  </Button>
                </div>
              }
            >
              <Card sectioned>
                <FormLayout>
                  <TextField
                    label={i18n.translate("Template.Label.name")}
                    value={templateName}
                    onChange={handleChangeTemplateName}
                  />
                  <Label>{i18n.translate("Template.Label.code")}</Label>
                  <TextEditor
                    mode="html"
                    theme="github"
                    value={templateCode}
                    onChange={handleChangeTemplateCode}
                  />
                  <Checkbox
                    label={i18n.translate("Template.Label.printByDefault")}
                    checked={isDefault}
                    onChange={handleChangeDefault}
                  />
                </FormLayout>
              </Card>
            </Layout.AnnotatedSection>
          </Layout>
          {errorNotifications}
          <LiquidVariableReference
            viewLiquid={viewLiquid}
            handleViewLiquid={handleViewLiquid}
            i18n={i18n}
          />
          {!activeToast && isEdit && activePreview && isValidateForm() ? (
            <PreviewTemplateDialog
              i18n={i18n}
              templateCode={templateCode}
              templateId={templateId}
              templateName={templateName}
              handleActiveDialog={handleActiveDialog}
            />
          ) : null}
        </Page>
      </div>
    </Frame>
  );
}

export default TemplateForm;
