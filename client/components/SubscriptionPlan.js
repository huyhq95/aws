import { useQuery, gql } from "@apollo/react-hooks";
import { useState, useEffect, useCallback } from "react";
import {
  Page,
  Banner,
  SkeletonPage,
  SkeletonBodyText,
  Card,
} from "@shopify/polaris";
import { Context } from "@shopify/app-bridge-react";
import { Redirect } from "@shopify/app-bridge/actions";
import initApollo from "../utils/init-apollo";

const GET_SHOP_INFO_QUERY = gql`
  query {
    shopInfo
  }
`;

const GET_SUBSCRIPTION_URL_QUERY = gql`
  query {
    subscriptionUrl
  }
`;

const TIMEOUT = SUBSCRIPTION_LOADING_TIMEOUT || 30000;

function SubscriptionPlan({ i18n }) {
  let timer = null;
  const [isTimedOut, setIsTimedOut] = useState(false);
  const [isLoadingSubscriptionUrl, setLoadingSubscriptionUrl] = useState(false);
  const [subscriptionUrl, setSubscriptionUrl] = useState(null);

  const loadSubcriptionUrl = useCallback(async () => {
    const apolloClient = initApollo({});
    setLoadingSubscriptionUrl(true);
    const getSubscriptionUrlResponse = await apolloClient.query({
      query: GET_SUBSCRIPTION_URL_QUERY,
    });
    if (getSubscriptionUrlResponse && getSubscriptionUrlResponse.data) {
      setSubscriptionUrl(getSubscriptionUrlResponse.data.subscriptionUrl);
    }
  }, []);

  const {
    data: shopInfoData,
    loading: shopInfoLoading,
    startPolling: startPollingShopInfo,
    stopPolling: stopPollingShopInfo,
  } = useQuery(GET_SHOP_INFO_QUERY);

  const subscriptionStatus =
    shopInfoData &&
    shopInfoData.shopInfo &&
    shopInfoData.shopInfo.appSubscription
      ? shopInfoData.shopInfo.appSubscription.status
      : null;

  const subscriptionUpdated =
    shopInfoData &&
    shopInfoData.shopInfo &&
    shopInfoData.shopInfo.appSubscription &&
    shopInfoData.shopInfo.appSubscription.subscriptionUpdated
      ? shopInfoData.shopInfo.appSubscription.subscriptionUpdated
      : null;

  if (subscriptionStatus === "ACTIVE" || isTimedOut) {
    stopPollingShopInfo();
  }

  useEffect(() => {
    if (subscriptionStatus !== "ACTIVE") {
      startPollingShopInfo(parseInt(POLLING_INTERVAL));
    }
  }, [subscriptionStatus]);

  if ((shopInfoLoading || !subscriptionUpdated) && !isTimedOut) {
    if (!timer) {
      timer = setTimeout(() => {
        clearTimeout(timer);
        setIsTimedOut(true);
      }, TIMEOUT);
    }
    return (
      <SkeletonPage title={``}>
        <Card.Section>
          <SkeletonBodyText />
        </Card.Section>
      </SkeletonPage>
    );
  }

  return (
    <React.Fragment>
      {"ACTIVE" !== subscriptionStatus ? (
        <Context.Consumer>
          {(app) => {
            if (app && subscriptionUrl) {
              const redirect = Redirect.create(app);
              redirect.dispatch(Redirect.Action.REMOTE, subscriptionUrl);
            }
            return (
              <Page fullWidth>
                <Banner
                  title={i18n.translate(
                    "SubscriptionPlan.SubscriptionBanner.title"
                  )}
                  status="critical"
                  action={{
                    content: i18n.translate(
                      "SubscriptionPlan.SubscriptionBanner.subscriptionPlan"
                    ),
                    loading: isLoadingSubscriptionUrl,
                    onAction: loadSubcriptionUrl,
                  }}
                >
                  <p>
                    {i18n.translate(
                      "SubscriptionPlan.SubscriptionBanner.description"
                    )}
                  </p>
                </Banner>
              </Page>
            );
          }}
        </Context.Consumer>
      ) : null}
    </React.Fragment>
  );
}

export default SubscriptionPlan;
