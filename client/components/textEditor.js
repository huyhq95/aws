import brace from "brace";
import "brace/mode/html";
import "brace/theme/github";
import AceEditor from "react-ace";

const textEditor = (props) => (
  <div>
    <AceEditor
      mode={props.mode}
      theme={props.theme}
      onChange={props.onChange}
      value={props.value}
      width="100%"
    />
  </div>
);

export default textEditor;
