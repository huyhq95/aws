import { useState, useCallback, useEffect } from "react";
import {
  Modal,
  Layout,
  SkeletonPage,
  SkeletonBodyText,
  Card,
  ChoiceList,
} from "@shopify/polaris";
import { useQuery, useApolloClient } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { find, isEmpty, remove } from "lodash";
import parse from "html-react-parser";

const GET_PREVIEW_TEMPLATE_QUERY = gql`
  query getPreviewTemplate($templateCode: String, $orderId: String) {
    getPreviewTemplate(templateCode: $templateCode, orderId: $orderId) {
      message
      success
      data {
        previewTemplateCode
      }
    }
  }
`;

const GET_ORDERS = gql`
  query {
    getOrders {
      success
      orders {
        id
        name
      }
    }

    getTemplatesByShop {
      success
      data {
        _id
        templateName
        templateCode
      }
    }
  }
`;

export default function previewTemplateDialog(props) {
  const { i18n, templateCode, templateId, templateName } = props;
  const [modalActive, setModalActive] = useState(true);
  const [isLoading, setLoading] = useState(true);
  const [listOrders, setListOrders] = useState(null);
  const [listTemplate, setListTemplate] = useState(null);
  const [selectedOrder, setSelectedOrder] = useState(null);
  const [selectedTemplate, setSelectedTemplate] = useState(null);
  const [previewTemplateCode, setPreviewTemplateCode] = useState(null);
  const client = useApolloClient();

  useQuery(GET_ORDERS, {
    onCompleted: async (data) => {
      const { orders, success } = data.getOrders;
      if (success) {
        setListOrders(orders);
        setSelectedOrder(orders.length > 0 ? orders[0].id : null);
      }
      if (data.getTemplatesByShop.success) {
        let templates = data.getTemplatesByShop.data;
        let selected = find(templates, function (o) {
          return o._id == templateId;
        });
        remove(templates, function (o) {
          return o._id == templateId;
        });
        templates.unshift(selected);
        setSelectedTemplate(templateId);
        setListTemplate(templates);
      }
    },
  });

  useEffect(() => {
    (async () => {
      if (!isEmpty(selectedOrder) && !isEmpty(selectedTemplate)) {
        setLoading(true);
        let selectedTemplateCode = getTemplateCode();
        let respone = await client.query({
          query: GET_PREVIEW_TEMPLATE_QUERY,
          variables: {
            templateCode: selectedTemplateCode,
            orderId: selectedOrder,
          },
        });
        if (
          respone &&
          respone.data &&
          respone.data.getPreviewTemplate &&
          respone.data.getPreviewTemplate.data
        ) {
          setPreviewTemplateCode(
            respone.data.getPreviewTemplate.data.previewTemplateCode
          );
        }
        if (client) {
          setLoading(false);
          await client.stop();
          await client.resetStore();
        }
      }
    })();
  }, [selectedOrder, selectedTemplate]);

  const handleModalChange = useCallback(() => setModalActive(!modalActive), [
    modalActive,
  ]);

  const handleClose = () => {
    handleModalChange();
    props.handleActiveDialog();
  };

  const handleChangeTemplate = useCallback(
    (value) => setSelectedTemplate(value[0]),
    []
  );
  const handleChangeOrder = useCallback(
    (value) => setSelectedOrder(value[0]),
    []
  );

  function formatOrders() {
    let orders = [];
    listOrders.forEach(function (e, k) {
      orders.push({
        label: e.name,
        value: e.id,
      });
    });
    return orders;
  }

  function formatTemplates() {
    let templates = [];
    listTemplate.forEach(function (e, k) {
      templates.push({
        label: e.templateName,
        value: e._id,
      });
    });
    return templates;
  }

  function getTemplateCode() {
    let selected = find(listTemplate, function (o) {
      return o._id == selectedTemplate;
    });
    return selected._id == templateId ? templateCode : selected.templateCode;
  }

  return (
    <div>
      <Modal
        large
        open={modalActive}
        onClose={handleClose}
        title={i18n.translate("Template.Label.previewTemplates")}
        secondaryActions={[
          {
            content: i18n.translate("Button.cancel"),
            onAction: handleClose,
          },
        ]}
      >
        <Modal.Section>
          {isLoading ? (
            <SkeletonPage fullWidth>
              <Card.Section>
                <SkeletonBodyText />
              </Card.Section>
            </SkeletonPage>
          ) : (
            <Layout>
              <Layout.Section>
                <Card title={templateName} sectioned>
                  {parse(previewTemplateCode)}
                </Card>
              </Layout.Section>
              <Layout.Section secondary>
                <Card
                  title={i18n.translate("Template.Label.templates")}
                  sectioned
                >
                  <ChoiceList
                    choices={formatTemplates()}
                    selected={selectedTemplate}
                    onChange={handleChangeTemplate}
                  />
                </Card>
                <Card title={i18n.translate("Template.Label.orders")} sectioned>
                  <ChoiceList
                    choices={formatOrders()}
                    selected={selectedOrder}
                    onChange={handleChangeOrder}
                  />
                </Card>
              </Layout.Section>
            </Layout>
          )}
        </Modal.Section>
      </Modal>
    </div>
  );
}
