import React, { useState, useEffect } from "react";
import { Card, Tabs } from "@shopify/polaris";
import { useRouter } from "next/router";

function NavigationTabs() {
  const router = useRouter();
  const [selectedTab, setSelectedTab] = useState(0);

  const tabs = [
    {
      id: "Templates",
      content: "Templates",
      path: "/admin/templates",
    },
    {
      id: "Configuration",
      content: "Configuration",
      path: "/admin/configuration",
    },
  ];

  const handleTabChange = (tabId) => {
    setSelectedTab(tabId);
    router.push(tabs[tabId].path);
  };

  useEffect(() => {
    let tabIndex = tabs.findIndex(
      (tab) =>
        tab.path === router.route ||
        (tab.pathChild && tab.pathChild.includes(router.route))
    );
    if (tabIndex !== -1) setSelectedTab(tabIndex);
  }, []);

  return (
    <React.Fragment>
      <Card>
        <Tabs
          tabs={tabs}
          selected={selectedTab}
          onSelect={handleTabChange}
        ></Tabs>
      </Card>
    </React.Fragment>
  );
}
export default NavigationTabs;
