exports.ids = [10];
exports.modules = {

/***/ "bNi+":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var brace__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("kq1J");
/* harmony import */ var brace__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(brace__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var brace_mode_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("HwQy");
/* harmony import */ var brace_mode_html__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(brace_mode_html__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var brace_theme_github__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("cayI");
/* harmony import */ var brace_theme_github__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(brace_theme_github__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_ace__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("9rux");
/* harmony import */ var react_ace__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_ace__WEBPACK_IMPORTED_MODULE_4__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const textEditor = props => __jsx("div", null, __jsx(react_ace__WEBPACK_IMPORTED_MODULE_4___default.a, {
  mode: props.mode,
  theme: props.theme,
  onChange: props.onChange,
  value: props.value,
  width: "100%"
}));

/* harmony default export */ __webpack_exports__["default"] = (textEditor);

/***/ })

};;