module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+U72":
/***/ (function(module) {

module.exports = JSON.parse("{\"Polaris\":{\"Actions\":{\"moreActions\":\"More actions\"},\"Avatar\":{\"label\":\"Avatar\",\"labelWithInitials\":\"Avatar with initials {initials}\"},\"Autocomplete\":{\"spinnerAccessibilityLabel\":\"Loading\"},\"Badge\":{\"PROGRESS_LABELS\":{\"incomplete\":\"Incomplete\",\"partiallyComplete\":\"Partially complete\",\"complete\":\"Complete\"},\"STATUS_LABELS\":{\"info\":\"Info\",\"success\":\"Success\",\"warning\":\"Warning\",\"critical\":\"Critical\",\"attention\":\"Attention\",\"new\":\"New\"}},\"Button\":{\"spinnerAccessibilityLabel\":\"Loading\",\"connectedDisclosureAccessibilityLabel\":\"Related actions\"},\"Common\":{\"checkbox\":\"checkbox\",\"undo\":\"Undo\",\"cancel\":\"Cancel\",\"newWindowAccessibilityHint\":\"(opens a new window)\",\"clear\":\"Clear\",\"close\":\"Close\",\"submit\":\"Submit\",\"more\":\"More\"},\"ContextualSaveBar\":{\"save\":\"Save\",\"discard\":\"Discard\"},\"DataTable\":{\"sortAccessibilityLabel\":\"sort {direction} by\",\"navAccessibilityLabel\":\"Scroll table {direction} one column\",\"totalsRowHeading\":\"Totals\",\"totalRowHeading\":\"Total\"},\"DatePicker\":{\"previousMonth\":\"Show previous month, {previousMonthName} {showPreviousYear}\",\"nextMonth\":\"Show next month, {nextMonth} {nextYear}\",\"today\":\"Today \",\"months\":{\"january\":\"January\",\"february\":\"February\",\"march\":\"March\",\"april\":\"April\",\"may\":\"May\",\"june\":\"June\",\"july\":\"July\",\"august\":\"August\",\"september\":\"September\",\"october\":\"October\",\"november\":\"November\",\"december\":\"December\"},\"days\":{\"monday\":\"Monday\",\"tuesday\":\"Tuesday\",\"wednesday\":\"Wednesday\",\"thursday\":\"Thursday\",\"friday\":\"Friday\",\"saturday\":\"Saturday\",\"sunday\":\"Sunday\"},\"daysAbbreviated\":{\"monday\":\"Mo\",\"tuesday\":\"Tu\",\"wednesday\":\"We\",\"thursday\":\"Th\",\"friday\":\"Fr\",\"saturday\":\"Sa\",\"sunday\":\"Su\"}},\"DiscardConfirmationModal\":{\"title\":\"Discard all unsaved changes\",\"message\":\"If you discard changes, you’ll delete any edits you made since you last saved.\",\"primaryAction\":\"Discard changes\",\"secondaryAction\":\"Continue editing\"},\"DropZone\":{\"overlayTextFile\":\"Drop file to upload\",\"overlayTextImage\":\"Drop image to upload\",\"errorOverlayTextFile\":\"File type is not valid\",\"errorOverlayTextImage\":\"Image type is not valid\",\"FileUpload\":{\"actionTitleFile\":\"Add file\",\"actionTitleImage\":\"Add image\",\"actionHintFile\":\"or drop files to upload\",\"actionHintImage\":\"or drop images to upload\",\"label\":\"Upload file\"}},\"EmptySearchResult\":{\"altText\":\"Empty search results\"},\"Frame\":{\"skipToContent\":\"Skip to content\",\"Navigation\":{\"closeMobileNavigationLabel\":\"Close navigation\"}},\"Icon\":{\"backdropWarning\":\"The {color} icon doesn’t accept backdrops. The icon colors that have backdrops are: {colorsWithBackDrops}\"},\"ActionMenu\":{\"RollupActions\":{\"rollupButton\":\"Actions\"}},\"Filters\":{\"moreFilters\":\"More filters\",\"moreFiltersWithCount\":\"More filters ({count})\",\"filter\":\"Filter {resourceName}\",\"noFiltersApplied\":\"No filters applied\",\"cancel\":\"Cancel\",\"done\":\"Done\",\"clearAllFilters\":\"Clear all filters\",\"clear\":\"Clear\",\"clearLabel\":\"Clear {filterName}\"},\"Modal\":{\"iFrameTitle\":\"body markup\",\"modalWarning\":\"These required properties are missing from Modal: {missingProps}\"},\"Pagination\":{\"previous\":\"Previous\",\"next\":\"Next\",\"pagination\":\"Pagination\"},\"ProgressBar\":{\"negativeWarningMessage\":\"Values passed to the progress prop shouldn’t be negative. Resetting {progress} to 0.\",\"exceedWarningMessage\":\"Values passed to the progress prop shouldn’t exceed 100. Setting {progress} to 100.\"},\"ResourceList\":{\"sortingLabel\":\"Sort by\",\"defaultItemSingular\":\"item\",\"defaultItemPlural\":\"items\",\"showing\":\"Showing {itemsCount} {resource}\",\"showingTotalCount\":\"Showing {itemsCount} of {totalItemsCount} {resource}\",\"loading\":\"Loading {resource}\",\"selected\":\"{selectedItemsCount} selected\",\"allItemsSelected\":\"All {itemsLength}+ {resourceNamePlural} in your store are selected.\",\"allFilteredItemsSelected\":\"All {itemsLength}+ {resourceNamePlural} in this filter are selected.\",\"selectAllItems\":\"Select all {itemsLength}+ {resourceNamePlural} in your store\",\"selectAllFilteredItems\":\"Select all {itemsLength}+ {resourceNamePlural} in this filter\",\"emptySearchResultTitle\":\"No {resourceNamePlural} found\",\"emptySearchResultDescription\":\"Try changing the filters or search term\",\"selectButtonText\":\"Select\",\"a11yCheckboxDeselectAllSingle\":\"Deselect {resourceNameSingular}\",\"a11yCheckboxSelectAllSingle\":\"Select {resourceNameSingular}\",\"a11yCheckboxDeselectAllMultiple\":\"Deselect all {itemsLength} {resourceNamePlural}\",\"a11yCheckboxSelectAllMultiple\":\"Select all {itemsLength} {resourceNamePlural}\",\"ariaLiveSingular\":\"{itemsLength} item\",\"ariaLivePlural\":\"{itemsLength} items\",\"Item\":{\"actionsDropdownLabel\":\"Actions for {accessibilityLabel}\",\"actionsDropdown\":\"Actions dropdown\",\"viewItem\":\"View details for {itemName}\"},\"BulkActions\":{\"actionsActivatorLabel\":\"Actions\",\"moreActionsActivatorLabel\":\"More actions\",\"warningMessage\":\"To provide a better user experience. There should only be a maximum of {maxPromotedActions} promoted actions.\"},\"FilterCreator\":{\"filterButtonLabel\":\"Filter\",\"selectFilterKeyPlaceholder\":\"Select a filter…\",\"addFilterButtonLabel\":\"Add filter\",\"showAllWhere\":\"Show all {resourceNamePlural} where:\"},\"FilterControl\":{\"textFieldLabel\":\"Search {resourceNamePlural}\"},\"FilterValueSelector\":{\"selectFilterValuePlaceholder\":\"Select a filter…\"},\"DateSelector\":{\"dateFilterLabel\":\"Select a value\",\"dateValueLabel\":\"Date\",\"dateValueError\":\"Match YYYY-MM-DD format\",\"dateValuePlaceholder\":\"YYYY-MM-DD\",\"SelectOptions\":{\"PastWeek\":\"in the last week\",\"PastMonth\":\"in the last month\",\"PastQuarter\":\"in the last 3 months\",\"PastYear\":\"in the last year\",\"ComingWeek\":\"next week\",\"ComingMonth\":\"next month\",\"ComingQuarter\":\"in the next 3 months\",\"ComingYear\":\"in the next year\",\"OnOrBefore\":\"on or before\",\"OnOrAfter\":\"on or after\"},\"FilterLabelForValue\":{\"past_week\":\"in the last week\",\"past_month\":\"in the last month\",\"past_quarter\":\"in the last 3 months\",\"past_year\":\"in the last year\",\"coming_week\":\"next week\",\"coming_month\":\"next month\",\"coming_quarter\":\"in the next 3 months\",\"coming_year\":\"in the next year\",\"on_or_before\":\"before {date}\",\"on_or_after\":\"after {date}\"}}},\"SkeletonPage\":{\"loadingLabel\":\"Page loading\"},\"Spinner\":{\"warningMessage\":\"The color {color} is not meant to be used on {size} spinners. The colors available on large spinners are: {colors}\"},\"Tabs\":{\"toggleTabsLabel\":\"More tabs\"},\"Tag\":{\"ariaLabel\":\"Remove {children}\"},\"TextField\":{\"characterCount\":\"{count} characters\",\"characterCountWithMaxLength\":\"{count} of {limit} characters used\"},\"TopBar\":{\"toggleMenuLabel\":\"Toggle menu\",\"SearchField\":{\"clearButtonLabel\":\"Clear\",\"search\":\"Search\"}},\"MediaCard\":{\"popoverButton\":\"Actions\"},\"VideoThumbnail\":{\"playButtonA11yLabel\":{\"default\":\"Play video\",\"defaultWithDuration\":\"Play video of length {duration}\",\"duration\":{\"hours\":{\"other\":{\"only\":\"{hourCount} hours\",\"andMinutes\":\"{hourCount} hours and {minuteCount} minutes\",\"andMinute\":\"{hourCount} hours and {minuteCount} minute\",\"minutesAndSeconds\":\"{hourCount} hours, {minuteCount} minutes, and {secondCount} seconds\",\"minutesAndSecond\":\"{hourCount} hours, {minuteCount} minutes, and {secondCount} second\",\"minuteAndSeconds\":\"{hourCount} hours, {minuteCount} minute, and {secondCount} seconds\",\"minuteAndSecond\":\"{hourCount} hours, {minuteCount} minute, and {secondCount} second\",\"andSeconds\":\"{hourCount} hours and {secondCount} seconds\",\"andSecond\":\"{hourCount} hours and {secondCount} second\"},\"one\":{\"only\":\"{hourCount} hour\",\"andMinutes\":\"{hourCount} hour and {minuteCount} minutes\",\"andMinute\":\"{hourCount} hour and {minuteCount} minute\",\"minutesAndSeconds\":\"{hourCount} hour, {minuteCount} minutes, and {secondCount} seconds\",\"minutesAndSecond\":\"{hourCount} hour, {minuteCount} minutes, and {secondCount} second\",\"minuteAndSeconds\":\"{hourCount} hour, {minuteCount} minute, and {secondCount} seconds\",\"minuteAndSecond\":\"{hourCount} hour, {minuteCount} minute, and {secondCount} second\",\"andSeconds\":\"{hourCount} hour and {secondCount} seconds\",\"andSecond\":\"{hourCount} hour and {secondCount} second\"}},\"minutes\":{\"other\":{\"only\":\"{minuteCount} minutes\",\"andSeconds\":\"{minuteCount} minutes and {secondCount} seconds\",\"andSecond\":\"{minuteCount} minutes and {secondCount} second\"},\"one\":{\"only\":\"{minuteCount} minute\",\"andSeconds\":\"{minuteCount} minute and {secondCount} seconds\",\"andSecond\":\"{minuteCount} minute and {secondCount} second\"}},\"seconds\":{\"other\":\"{secondCount} seconds\",\"one\":\"{secondCount} second\"}}}}}}");

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("1TCz");


/***/ }),

/***/ "1TCz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("mU8t");
/* harmony import */ var _apollo_react_hooks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("8Bbg");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("nj/N");
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_shopify_polaris__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("ZQgG");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("vmXh");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _shopify_polaris_dist_styles_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("l7Dk");
/* harmony import */ var _shopify_polaris_dist_styles_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_shopify_polaris_dist_styles_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _shopify_polaris_locales_en_json__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("+U72");
var _shopify_polaris_locales_en_json__WEBPACK_IMPORTED_MODULE_7___namespace = /*#__PURE__*/__webpack_require__.t("+U72", 1);
/* harmony import */ var _shopify_polaris_locales_ja_json__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("Ti7g");
var _shopify_polaris_locales_ja_json__WEBPACK_IMPORTED_MODULE_8___namespace = /*#__PURE__*/__webpack_require__.t("Ti7g", 1);
/* harmony import */ var _shopify_react_i18n__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("lYiN");
/* harmony import */ var _shopify_react_i18n__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_react_i18n__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _client_utils_init_apollo__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("o9hV");
/* harmony import */ var _client_styles_app_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("eTVe");
/* harmony import */ var _client_styles_app_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_client_styles_app_css__WEBPACK_IMPORTED_MODULE_11__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;











const apolloClient = Object(_client_utils_init_apollo__WEBPACK_IMPORTED_MODULE_10__[/* default */ "a"])({});

class MyApp extends next_app__WEBPACK_IMPORTED_MODULE_2___default.a {
  render() {
    const {
      Component,
      pageProps
    } = this.props;
    const shopOrigin = js_cookie__WEBPACK_IMPORTED_MODULE_5___default.a.get("shopOrigin");
    const locale = js_cookie__WEBPACK_IMPORTED_MODULE_5___default.a.get("locale") ? js_cookie__WEBPACK_IMPORTED_MODULE_5___default.a.get("locale") : "en-US";
    const translation = locale.indexOf("ja") == 0 ? _shopify_polaris_locales_ja_json__WEBPACK_IMPORTED_MODULE_8__ : _shopify_polaris_locales_en_json__WEBPACK_IMPORTED_MODULE_7__;
    const i18nManager = new _shopify_react_i18n__WEBPACK_IMPORTED_MODULE_9__["I18nManager"]({
      locale,

      onError(error) {
        console.log(error);
      }

    });
    return __jsx(_shopify_react_i18n__WEBPACK_IMPORTED_MODULE_9__["I18nContext"].Provider, {
      value: i18nManager
    }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_3__["AppProvider"], {
      i18n: translation
    }, __jsx(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_4__["Provider"], {
      config: {
        apiKey: "c1ff4bbd61b865722e07b9495a53c7e6",
        shopOrigin: shopOrigin,
        forceRedirect: true
      }
    }, __jsx(_apollo_react_hooks__WEBPACK_IMPORTED_MODULE_1__["ApolloProvider"], {
      client: apolloClient
    }, __jsx(Component, pageProps)))));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (MyApp);

/***/ }),

/***/ "8Bbg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("B5Ud")


/***/ }),

/***/ "AroE":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "B5Ud":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("AroE");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _utils = __webpack_require__("kYf9");

exports.AppInitialProps = _utils.AppInitialProps;
exports.NextWebVitalsMetric = _utils.NextWebVitalsMetric;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

async function appGetInitialProps({
  Component,
  ctx
}) {
  const pageProps = await (0, _utils.loadGetInitialProps)(Component, ctx);
  return {
    pageProps
  };
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    const {
      router,
      Component,
      pageProps,
      __N_SSG,
      __N_SSP
    } = this.props;
    return /*#__PURE__*/_react.default.createElement(Component, Object.assign({}, pageProps, // we don't add the legacy URL prop if it's using non-legacy
    // methods like getStaticProps and getServerSideProps
    !(__N_SSG || __N_SSP) ? {
      url: createUrl(router)
    } : {}));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
let warnContainer;
let warnUrl;

if (false) {} // @deprecated noop for now until removal


function Container(p) {
  if (false) {}
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  const {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (false) {}
      return query;
    },

    get pathname() {
      if (false) {}
      return pathname;
    },

    get asPath() {
      if (false) {}
      return asPath;
    },

    back: () => {
      if (false) {}
      router.back();
    },
    push: (url, as) => {
      if (false) {}
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (false) {}
      const pushRoute = as ? href : '';
      const pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (false) {}
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (false) {}
      const replaceRoute = as ? href : '';
      const replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "Ti7g":
/***/ (function(module) {

module.exports = JSON.parse("{\"Polaris\":{\"Avatar\":{\"label\":\"アバター\",\"labelWithInitials\":\"頭文字が{initials}のアバター\"},\"Autocomplete\":{\"spinnerAccessibilityLabel\":\"読み込んでいます\"},\"Badge\":{\"PROGRESS_LABELS\":{\"incomplete\":\"未完了\",\"partiallyComplete\":\"一部完了済\",\"complete\":\"完了\"},\"STATUS_LABELS\":{\"info\":\"情報\",\"success\":\"成功\",\"warning\":\"警告\",\"attention\":\"注意\",\"new\":\"新規\",\"critical\":\"クリティカル\"}},\"Button\":{\"spinnerAccessibilityLabel\":\"読み込んでいます\",\"connectedDisclosureAccessibilityLabel\":\"関連したアクション\"},\"Common\":{\"checkbox\":\"チェックボックス\",\"undo\":\"元に戻す\",\"cancel\":\"キャンセルする\",\"newWindowAccessibilityHint\":\"(新しいウィンドウを開く)\",\"clear\":\"クリア\",\"close\":\"閉じる\",\"submit\":\"送信する\",\"more\":\"さらに\"},\"ContextualSaveBar\":{\"save\":\"保存する\",\"discard\":\"破棄する\"},\"DataTable\":{\"sortAccessibilityLabel\":\"で{direction}を並び替える\",\"navAccessibilityLabel\":\"表{direction}を1列スクロールする\",\"totalsRowHeading\":\"合計\",\"totalRowHeading\":\"合計\"},\"DatePicker\":{\"previousMonth\":\"先月{showPreviousYear}{previousMonthName}を表示する\",\"nextMonth\":\"来月{nextYear}{nextMonth}を表示する\",\"today\":\"今日 \",\"months\":{\"january\":\"1月\",\"february\":\"2月\",\"march\":\"3月\",\"april\":\"4月\",\"may\":\"5月\",\"june\":\"6月\",\"july\":\"7月\",\"august\":\"8月\",\"september\":\"9月\",\"october\":\"10月\",\"november\":\"11月\",\"december\":\"12月\"},\"daysAbbreviated\":{\"monday\":\"月\",\"tuesday\":\"火\",\"wednesday\":\"水\",\"thursday\":\"木\",\"friday\":\"金\",\"saturday\":\"土\",\"sunday\":\"日\"},\"days\":{\"monday\":\"月曜日\",\"tuesday\":\"火曜日\",\"wednesday\":\"水曜日\",\"thursday\":\"木曜日\",\"friday\":\"金曜日\",\"saturday\":\"土曜日\",\"sunday\":\"日曜日\"}},\"DiscardConfirmationModal\":{\"title\":\"すべての保存されていない変更を破棄する\",\"message\":\"変更を破棄すると、最後に保存した時以降のすべての編集が削除されます。\",\"primaryAction\":\"変更を破棄する\",\"secondaryAction\":\"編集を続ける\"},\"DropZone\":{\"overlayTextFile\":\"ファイルをドロップしてアップロード\",\"overlayTextImage\":\"画像をドロップしてアップロードする\",\"errorOverlayTextFile\":\"ファイルタイプが有効ではありません\",\"errorOverlayTextImage\":\"画像タイプが有効ではありません\",\"FileUpload\":{\"actionTitleFile\":\"ファイルを追加する\",\"actionTitleImage\":\"画像を追加する\",\"actionHintFile\":\"または、ファイルをドロップしてアップロード\",\"actionHintImage\":\"または、画像をドロップしてアップロードする\",\"label\":\"ファイルをアップロード\"}},\"EmptySearchResult\":{\"altText\":\"空の検索結果\"},\"Frame\":{\"skipToContent\":\"コンテンツにスキップする\",\"Navigation\":{\"closeMobileNavigationLabel\":\"メニューを閉じる\"}},\"Icon\":{\"backdropWarning\":\"{color}のアイコンは、背景を受け付けません。背景を持つアイコンの色は次のとおりです: {colorsWithBackDrops}\"},\"ActionMenu\":{\"RollupActions\":{\"rollupButton\":\"アクション\"}},\"Filters\":{\"moreFilters\":\"詳細な絞り込み\",\"filter\":\"フィルター{resourceName}\",\"noFiltersApplied\":\"絞り込みが適用されていません\",\"cancel\":\"キャンセルする\",\"done\":\"完了\",\"clearAllFilters\":\"すべての絞り込みをクリアする\",\"clear\":\"クリア\",\"clearLabel\":\"{filterName}をクリアする\",\"moreFiltersWithCount\":\"詳細な絞り込み ({count})\"},\"Modal\":{\"iFrameTitle\":\"ボディマークアップ\",\"modalWarning\":\"これらの必要なプロパティがモーダルにありません: {missingProps}\"},\"Pagination\":{\"previous\":\"前へ\",\"next\":\"次へ\",\"pagination\":\"ページネーション\"},\"ProgressBar\":{\"negativeWarningMessage\":\"進行中のプロパティに渡される値にマイナスは使用できません。{progress}を0にリセットする。\",\"exceedWarningMessage\":\"進行中のプロパティに渡される値は100を超えることはできません。{progress}を100に設定する。\"},\"ResourceList\":{\"sortingLabel\":\"並び替え\",\"defaultItemSingular\":\"アイテム\",\"defaultItemPlural\":\"アイテム\",\"showing\":\"{itemsCount}個の{resource}を表示しています\",\"loading\":\"{resource}を読み込んでいます\",\"selected\":\"{selectedItemsCount}個を選択済\",\"allItemsSelected\":\"ストアにあるすべての{itemsLength}{resourceNamePlural}が選択されています。\",\"selectAllItems\":\"ストアにあるすべての{itemsLength}{resourceNamePlural}を選択する\",\"emptySearchResultTitle\":\"{resourceNamePlural}が見つかりませんでした\",\"emptySearchResultDescription\":\"絞り込みや検索語を変更してください\",\"selectButtonText\":\"選択する\",\"a11yCheckboxDeselectAllSingle\":\"{resourceNameSingular}の選択を解除する\",\"a11yCheckboxSelectAllSingle\":\"{resourceNameSingular}を選択する\",\"a11yCheckboxDeselectAllMultiple\":\"すべての{itemsLength}{resourceNamePlural}の選択を解除する\",\"a11yCheckboxSelectAllMultiple\":\"すべての{itemsLength}{resourceNamePlural}を選択する\",\"ariaLiveSingular\":\"{itemsLength}個のアイテム\",\"ariaLivePlural\":\"{itemsLength}個のアイテム\",\"Item\":{\"actionsDropdownLabel\":\"{accessibilityLabel}のアクション\",\"actionsDropdown\":\"アクションドロップダウン\",\"viewItem\":\"{itemName}の詳細を表示する\"},\"BulkActions\":{\"actionsActivatorLabel\":\"アクション\",\"moreActionsActivatorLabel\":\"その他の操作\",\"warningMessage\":\"さらによいユーザーエクスペリエンスのために。宣伝のアクションは最大で{maxPromotedActions}回までです。\"},\"FilterCreator\":{\"filterButtonLabel\":\"絞り込む\",\"selectFilterKeyPlaceholder\":\"絞り込みを選択する...\",\"addFilterButtonLabel\":\"絞り込みを追加する\",\"showAllWhere\":\"すべての{resourceNamePlural}を表示する:\"},\"FilterControl\":{\"textFieldLabel\":\"{resourceNamePlural}を検索する\"},\"FilterValueSelector\":{\"selectFilterValuePlaceholder\":\"絞り込みを選択する...\"},\"DateSelector\":{\"dateFilterLabel\":\"値を選択する\",\"dateValueLabel\":\"日付\",\"dateValueError\":\"YYYY年MM月DD日フォーマットと一致する\",\"dateValuePlaceholder\":\"YYYY年MM月DD日\",\"SelectOptions\":{\"PastWeek\":\"先週\",\"PastMonth\":\"先月\",\"PastQuarter\":\"過去3か月\",\"PastYear\":\"昨年\",\"ComingWeek\":\"来週\",\"ComingMonth\":\"来月\",\"ComingQuarter\":\"今後3か月\",\"ComingYear\":\"来年\",\"OnOrBefore\":\"以前\",\"OnOrAfter\":\"以後\"},\"FilterLabelForValue\":{\"past_week\":\"先週\",\"past_month\":\"先月\",\"past_quarter\":\"過去3か月\",\"past_year\":\"昨年\",\"coming_week\":\"来週\",\"coming_month\":\"来月\",\"coming_quarter\":\"今後3か月\",\"coming_year\":\"来年\",\"on_or_before\":\"{date}以前\",\"on_or_after\":\"{date}以後\"}},\"showingTotalCount\":\"{totalItemsCount}件の{resource}中、{itemsCount}件を表示中\",\"allFilteredItemsSelected\":\"この絞り込みの{itemsLength}と{resourceNamePlural}がすべて選択されます。\",\"selectAllFilteredItems\":\"この絞り込みの{itemsLength}と{resourceNamePlural}をすべて選択する\"},\"SkeletonPage\":{\"loadingLabel\":\"ページの読み込み中\"},\"Spinner\":{\"warningMessage\":\"{color}色は{size}のスピナーでは使用されていません。大きなスピナーで利用可能な色は次のとおりです: {colors}\"},\"Tabs\":{\"toggleTabsLabel\":\"その他のタブ\"},\"Tag\":{\"ariaLabel\":\"{children}を削除する\"},\"TextField\":{\"characterCount\":\"{count}文字\",\"characterCountWithMaxLength\":\"{count}/{limit}文字使用\"},\"TopBar\":{\"toggleMenuLabel\":\"メニューを切り替える\",\"SearchField\":{\"clearButtonLabel\":\"クリア\",\"search\":\"検索\"}},\"MediaCard\":{\"popoverButton\":\"アクション\"},\"VideoThumbnail\":{\"playButtonA11yLabel\":{\"default\":\"ビデオを見る\",\"defaultWithDuration\":\"長さ{duration}の動画を再生する\",\"duration\":{\"hours\":{\"other\":{\"only\":\"{hourCount}時間\",\"andMinutes\":\"{hourCount}時間{minuteCount}分\",\"andMinute\":\"{hourCount}時間{minuteCount}分\",\"minutesAndSeconds\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minutesAndSecond\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minuteAndSeconds\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minuteAndSecond\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"andSeconds\":\"{hourCount}時間{secondCount}秒\",\"andSecond\":\"{hourCount}時間{secondCount}秒\"},\"one\":{\"only\":\"{hourCount}時間\",\"andMinutes\":\"{hourCount}時間{minuteCount}分\",\"andMinute\":\"{hourCount}時間{minuteCount}分\",\"minutesAndSeconds\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minutesAndSecond\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minuteAndSeconds\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"minuteAndSecond\":\"{hourCount}時間{minuteCount}分{secondCount}秒\",\"andSeconds\":\"{hourCount}時間{secondCount}秒\",\"andSecond\":\"{hourCount}時間{secondCount}秒\"}},\"minutes\":{\"other\":{\"only\":\"{minuteCount}分\",\"andSeconds\":\"{minuteCount}分{secondCount}秒\",\"andSecond\":\"{minuteCount}分{secondCount}秒\"},\"one\":{\"only\":\"{minuteCount}分\",\"andSeconds\":\"{minuteCount}分{secondCount}秒\",\"andSecond\":\"{minuteCount}分{secondCount}秒\"}},\"seconds\":{\"other\":\"{secondCount}秒\",\"one\":\"{secondCount}秒\"}}}},\"Actions\":{\"moreActions\":\"その他の操作\"}}}");

/***/ }),

/***/ "W+0S":
/***/ (function(module, exports) {

module.exports = require("isomorphic-fetch");

/***/ }),

/***/ "YvTO":
/***/ (function(module, exports) {

module.exports = require("apollo-boost");

/***/ }),

/***/ "ZQgG":
/***/ (function(module, exports) {

module.exports = require("@shopify/app-bridge-react");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "eTVe":
/***/ (function(module, exports) {



/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "l7Dk":
/***/ (function(module, exports) {



/***/ }),

/***/ "lYiN":
/***/ (function(module, exports) {

module.exports = require("@shopify/react-i18n");

/***/ }),

/***/ "mU8t":
/***/ (function(module, exports) {

module.exports = require("@apollo/react-hooks");

/***/ }),

/***/ "nj/N":
/***/ (function(module, exports) {

module.exports = require("@shopify/polaris");

/***/ }),

/***/ "o9hV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initApollo; });
/* harmony import */ var apollo_link__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("vuC2");
/* harmony import */ var apollo_link__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(apollo_link__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var apollo_boost__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("YvTO");
/* harmony import */ var apollo_boost__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(apollo_boost__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_fetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("W+0S");
/* harmony import */ var isomorphic_fetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_fetch__WEBPACK_IMPORTED_MODULE_2__);



let apolloClient = null; // Polyfill fetch() on the server (used by apollo-client)

if (true) {
  global.fetch = isomorphic_fetch__WEBPACK_IMPORTED_MODULE_2___default.a;
}

const shopifyApiLink = new apollo_boost__WEBPACK_IMPORTED_MODULE_1__["HttpLink"]({
  uri: "/shopify/graphql"
});
const adminApiLink = new apollo_boost__WEBPACK_IMPORTED_MODULE_1__["HttpLink"]({
  uri: "/admin/graphql"
});

function create(initialState) {
  return new apollo_boost__WEBPACK_IMPORTED_MODULE_1__["ApolloClient"]({
    connectToDevTools: false,
    ssrMode: !false,
    // Disables forceFetch on the server (so queries are only run once)
    link: apollo_link__WEBPACK_IMPORTED_MODULE_0__["ApolloLink"].split(operation => operation.getContext().clientName === "shopify", // Shopify GraphQL API
    shopifyApiLink, adminApiLink // Admin GraphQL API
    ),
    cache: new apollo_boost__WEBPACK_IMPORTED_MODULE_1__["InMemoryCache"]().restore(initialState || {}),
    fetchOptions: {
      credentials: "include"
    }
  });
}

function initApollo(initialState) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (true) {
    return create(initialState);
  } // Reuse client on the client-side


  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}

/***/ }),

/***/ "vmXh":
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "vuC2":
/***/ (function(module, exports) {

module.exports = require("apollo-link");

/***/ })

/******/ });