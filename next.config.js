require("dotenv").config();
const withCSS = require("@zeit/next-css");
const webpack = require("webpack");

module.exports = withCSS({
  webpack: (config) => {
    const env = {
      API_KEY: JSON.stringify(process.env.SHOPIFY_API_KEY),
      HOST: JSON.stringify(process.env.HOST),
      SUBSCRIPTION_LOADING_TIMEOUT: JSON.stringify(
        process.env.SUBSCRIPTION_LOADING_TIMEOUT
      ),
      MIN_TEMPLATE_NAME: JSON.stringify(process.env.MIN_TEMPLATE_NAME),
      MAX_TEMPLATE_NAME: JSON.stringify(process.env.MAX_TEMPLATE_NAME),
      POLLING_INTERVAL: JSON.stringify(process.env.POLLING_INTERVAL),
      CODE_PUT_THEME: JSON.stringify(process.env.CODE_PUT_THEME),
    };
    config.plugins.push(new webpack.DefinePlugin(env));
    return config;
  },
});
