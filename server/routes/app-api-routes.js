const { verifyRequest } = require("@shopify/koa-shopify-auth");
const graphqlServer = require("../app-api/graphql/graphql-server");
const Router = require("@koa/router");
const router = new Router();

router.all(
  "/admin/graphql",
  verifyRequest(),
  graphqlServer.getMiddleware({ path: "/admin/graphql" })
);

module.exports = router;
