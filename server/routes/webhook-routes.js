const Router = require("@koa/router");
const { receiveWebhook } = require("@shopify/koa-shopify-webhooks");

const handleAppSubscriptionUpdated = require("../app-api/webhooks/app-subscription-updated");
const handleAppUninsttalled = require("../app-api/webhooks/app-uninstalled");
const handleShopUpdated = require("../app-api/webhooks/shop-updated");
const handleThemesPublish = require("../app-api/webhooks/themes-publish");

const { SHOPIFY_API_SECRET, NODE_ENV } = process.env;
const isDevEnv = NODE_ENV === "development";
const webhook = receiveWebhook({ secret: SHOPIFY_API_SECRET });

const router = new Router();

router.post("/webhooks/app-subscription-updated", webhook, async (ctx) => {
  if (isDevEnv) {
    console.debug(ctx.state.webhook);
  }
  await handleAppSubscriptionUpdated(ctx, ctx.state.webhook);
});
router.post("/webhooks/app-uninstalled", webhook, async (ctx) => {
  if (isDevEnv) {
    console.debug(ctx.state.webhook);
  }
  await handleAppUninsttalled(ctx, ctx.state.webhook);
});
router.post("/webhooks/shop-updated", webhook, async (ctx) => {
  if (isDevEnv) {
    console.debug(ctx.state.webhook);
  }
  await handleShopUpdated(ctx, ctx.state.webhook);
});
router.post("/webhooks/themes-publish", webhook, async (ctx) => {
  if (isDevEnv) console.log(ctx.state.webhook);
  await handleThemesPublish(ctx, ctx.state.webhook);
});

module.exports = router;
