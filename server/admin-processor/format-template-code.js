import { Liquid } from "liquidjs";

module.exports.formatTemplateCode = async (orderInfo, templateCode) => {
  const engine = new Liquid();
  const liquidVariable = getLiquidVariable(orderInfo);
  return await engine.parseAndRender(templateCode, liquidVariable);
};

function getLiquidVariable(orderInfo) {
  const { order, shop, orderTransaction } = orderInfo;
  const { shipping_lines } = order;

  return {
    shop: shop,
    shop_name: shop.name,
    order_number: order.number,
    order_name: order.name,
    name: order.name,
    date: order.created_at,
    created_at: order.created_at,
    processed_at: order.processed_at,
    billing_address: order.billing_address,
    shipping_address: order.shipping_address,
    shipping_lines: order.shipping_lines,
    shipping_methods: order.shipping_lines,
    shipping_method: {
      title: order.shipping_lines[0] ? order.shipping_lines[0].title : "",
    },
    shipping_line: {
      title: order.shipping_lines[0] ? order.shipping_lines[0].title : "",
    },
    customer: order.customer,
    email: order.email,
    financial_status: order.financial_status,
    fulfillment_status: order.fulfillment_status,
    fulfillment_service: order.line_items[0].fulfillment_service,
    discounts_amount: order.total_discounts,
    discounts_savings:
      "-" + (parseFloat(order.total_discounts) * 100).toString(),
    gateways: order.payment_gateway_names,
    gateway: order.gateway,
    currency: order.currency,
    order_currency: order.currency,
    subtotal_price: order.subtotal_price,
    shipping_price: shipping_lines.reduce(
      (a, { price }) => a + parseFloat(price),
      0
    ),
    total_discounts: order.total_discounts,
    total_tax: order.total_tax,
    tax_price: order.total_tax,
    total_price: order.total_price,
    total_paid: orderTransaction.reduce(
      (a, { amount }) => a + parseFloat(amount),
      0
    ),
    transactions: orderTransaction,
    attributes: order.note_attributes,
    note: order.notes,
    line_items: order.line_items,
    refunds: order.refunds,
    tax_lines: order.shipping_lines[0]
      ? order.shipping_lines[0].tax_lines
      : null,
    discounts: order.discount_codes,
    fulfillments: order.fulfillments,
  };
}
