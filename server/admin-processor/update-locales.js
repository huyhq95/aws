import { getShopifyApi } from "../utils/get-shopify-api";
const dotenv = require("dotenv");
dotenv.config();
import i18n from "i18n";
import path from "path";

module.exports.updateLocales = async (shop, accessToken, key, activeTheme) => {
  const shopify = getShopifyApi(shop, accessToken);
  i18n.configure({
    locales: ["en", "ja"],
    defaultLocale: "en",
    directory: path.join(__dirname, "..", "..", "locales"),
    objectNotation: true,
  });
  i18n.setLocale(key || "en");

  let pathLocale =
    key == "en" ? `locales/${key}.default.json` : `locales/${key}.json`;
  let locales = await shopify.asset.get(activeTheme, {
    asset: {
      key: pathLocale,
    },
  });

  let locale = JSON.parse(locales.value);
  locale.easy_receipt = {
    print_receipt: i18n.__("EasyReceipt.printReceipt"),
    company_name: i18n.__("EasyReceipt.companyName"),
  };

  await shopify.asset.create(activeTheme, {
    key: pathLocale,
    value: JSON.stringify(locale),
  });
};
