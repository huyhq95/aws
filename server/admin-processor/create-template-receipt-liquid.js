import { getShopifyApi } from "../utils/get-shopify-api";

module.exports.createTemplateReceiptLiquid = async (
  shop,
  accessToken,
  key,
  activeTheme,
  templateCode
) => {
  const shopify = getShopifyApi(shop, accessToken);
  return await shopify.asset.create(activeTheme, {
    key,
    value: templateCode,
  });
};
