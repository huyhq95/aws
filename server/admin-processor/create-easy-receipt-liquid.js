import dotenv from "dotenv";
import { getShopifyApi } from "../utils/get-shopify-api";
dotenv.config();
const {
  TEMPLATES_RECEIPT_LIQUID_NAME,
  EASY_RECEIPT_VARIABLES_LIQUID,
  EASY_RECEIPT_VARIABLES_CONTROLS,
} = process.env;

module.exports.createEasyReceiptLiquid = async (
  shop,
  accessToken,
  activeTheme,
  easyReceiptVariables,
  templateReceiptLiquid,
  easyReceiptControls
) => {
  const shopify = getShopifyApi(shop, accessToken);
  await shopify.asset.create(activeTheme, {
    key: EASY_RECEIPT_VARIABLES_LIQUID,
    value: easyReceiptVariables,
  });
  await shopify.asset.create(activeTheme, {
    key: TEMPLATES_RECEIPT_LIQUID_NAME,
    value: templateReceiptLiquid,
  });
  await shopify.asset.create(activeTheme, {
    key: EASY_RECEIPT_VARIABLES_CONTROLS,
    value: easyReceiptControls,
  });
};
