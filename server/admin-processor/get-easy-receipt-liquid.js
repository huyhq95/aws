import dotenv from "dotenv";
import { getShopifyApi } from "../utils/get-shopify-api";
dotenv.config();
const {
  TEMPLATES_RECEIPT_LIQUID_NAME,
  EASY_RECEIPT_VARIABLES_LIQUID,
  EASY_RECEIPT_VARIABLES_CONTROLS,
} = process.env;

module.exports.getEasyReceiptLiquid = async (
  shop,
  accessToken,
  activeTheme
) => {
  const shopify = getShopifyApi(shop, accessToken);
  const easyReceiptVariables = await shopify.asset.get(activeTheme, {
    asset: {
      key: EASY_RECEIPT_VARIABLES_LIQUID,
    },
  });
  const templateReceiptLiquid = await shopify.asset.get(activeTheme, {
    asset: {
      key: TEMPLATES_RECEIPT_LIQUID_NAME,
    },
  });
  const easyReceiptControls = await shopify.asset.get(activeTheme, {
    asset: {
      key: EASY_RECEIPT_VARIABLES_CONTROLS,
    },
  });
  return {
    easyReceiptVariables: easyReceiptVariables && easyReceiptVariables.value,
    templateReceiptLiquid: templateReceiptLiquid && templateReceiptLiquid.value,
    easyReceiptControls: easyReceiptControls && easyReceiptControls.value,
  };
};
