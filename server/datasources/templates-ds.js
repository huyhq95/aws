const { DataSource } = require("apollo-datasource");
const { ObjectId } = require("koa-mongo");
const moment = require("moment-timezone");
class TemplatesDataSource extends DataSource {
  constructor() {
    super();
  }

  initialize(config) {
    this.koaContext = config.context;
    this.collection = this.koaContext.db.collection("templates");
  }

  async getTemplate(templateName, templateId, shopName) {
    return await this.collection.findOne({
      _id: { $ne: ObjectId(templateId) },
      templateName: { $regex: new RegExp("^" + templateName + "$", "i") },
      shopName,
    });
  }

  async saveTemplate(templateInput, templateId) {
    templateInput.createdTimeAt = moment().valueOf();
    await this.collection.updateOne(
      { _id: ObjectId(templateId) },
      { $set: templateInput },
      { upsert: true }
    );
  }

  async removeDefaultTemplate() {
    await this.collection.updateOne(
      {
        isDefault: true,
      },
      { $set: { isDefault: false } },
      { upsert: true }
    );
  }

  async getTemplatesByShop(shopName) {
    return await this.collection.find({ shopName: shopName }).toArray();
  }

  async getTemplateDefaultByShop(shopName) {
    return await this.collection.findOne({
      isDefault: true,
      shopName: shopName,
    });
  }

  async updateTemplateDefault(shopName, templateId, lastModifiled) {
    await this.collection.updateOne(
      { isDefault: true, shopName: shopName },
      { $set: { isDefault: false } }
    );
    await this.collection.updateOne(
      { _id: ObjectId(templateId), shopName: shopName },
      { $set: { isDefault: true, lastModifiled: lastModifiled } }
    );
  }

  async getTemplateById(templateId) {
    return await this.collection.findOne({ _id: ObjectId(templateId) });
  }

  async deleteTemplatesByShop(shopName) {
    let bulk = this.collection.initializeUnorderedBulkOp();
    bulk.find({ shopName: shopName }).remove({});
    await bulk.execute();
  }
}

module.exports = TemplatesDataSource;
