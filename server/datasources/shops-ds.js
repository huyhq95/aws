const { DataSource } = require("apollo-datasource");
const Cryptr = require("cryptr");
const dotenv = require("dotenv");
dotenv.config();

const getActiveSubscriptions = require("../store-api/admin-api/graphql/get-active-subscriptions");
const cryptr = new Cryptr(process.env.SHOPIFY_API_SECRET);

class ShopDataSource extends DataSource {
  constructor() {
    super();
  }

  initialize(config) {
    this.koaContext = config.context;
    this.collection = this.koaContext.db.collection("shops");
  }

  async saveSettings(session, settings) {
    const { shop } = session;

    const shopInfo = await this.getShopInfo(shop);

    // do not save settings if subscription is not active
    if (!shopInfo || "ACTIVE" !== shopInfo.appSubscription.status) {
      throw new Error("SubscriptionBanner.contentSubscriptionStatus");
    }

    await this.saveShopInfo(shop, { settings });
  }

  async deleteShop(shop) {
    let result = await this.collection.deleteOne({ shop: shop });
    return result.result.n > 0;
  }

  /**
   *
   * @param {*} shopInfo
   */
  async saveShopInfo(shop, shopInfo) {
    let clonedShopInfo = { ...shopInfo };

    if (clonedShopInfo.accessToken) {
      clonedShopInfo.accessToken = cryptr.encrypt(clonedShopInfo.accessToken);
    }

    await this.collection.updateOne(
      {
        shop: shop,
      },
      { $set: clonedShopInfo },
      { upsert: true }
    );
  }

  /**
   *
   * @param {*} shop
   * @param {*} shouldDecryptToken
   */
  async getShopInfo(shop, shouldDecryptToken = false) {
    let shopInfo = await this.collection.findOne({ shop });
    if (shopInfo && shopInfo.accessToken && shouldDecryptToken) {
      const decryptedAccessToken = cryptr.decrypt(shopInfo.accessToken);
      shopInfo.accessToken = decryptedAccessToken;
    }
    return shopInfo;
  }

  async updateActiveThemeByShop(shop, activeTheme) {
    await this.collection.updateOne(
      { shop: shop },
      {
        $set: {
          activeTheme: activeTheme,
        },
      }
    );
  }
}

module.exports = ShopDataSource;
