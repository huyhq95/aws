require("isomorphic-fetch");
const Shopify = require("shopify-api-node");
const { ApiVersion } = require("@shopify/koa-shopify-graphql-proxy");

const RECURRING_CREATE = `
  mutation CreateAppSubcription(
    $name: String!
    $lineItems: [AppSubscriptionLineItemInput!]!
    $test: Boolean
    $returnUrl: URL!
    $trialDays: Int
  ) {
    appSubscriptionCreate(
      name: $name
      lineItems: $lineItems
      test: $test
      returnUrl: $returnUrl
      trialDays: $trialDays
    ) {
      confirmationUrl
      appSubscription {
        id
        status
      }
      userErrors {
        field
        message
      }
    }
  }
`;

module.exports.getSubscriptionUrl = async (shop, accessToken) => {
  const {
    HOST,
    SUBSCRIPTION_PLAN_NAME,
    SUBSCRIPTION_AMOUNT,
    SUBSCRIPTION_TEST,
    SUBSCRIPTION_FREE_TRIAL,
  } = process.env;

  const lineItems = [
    {
      plan: {
        appRecurringPricingDetails: {
          price: {
            amount: SUBSCRIPTION_AMOUNT,
            currencyCode: "USD",
          },
        },
      },
    },
  ];

  const shopify = new Shopify({
    shopName: shop,
    accessToken: accessToken,
    apiVersion: ApiVersion.April20,
  });

  const variables = {
    name: SUBSCRIPTION_PLAN_NAME,
    lineItems: lineItems,
    test: !SUBSCRIPTION_TEST || SUBSCRIPTION_TEST.toLowerCase() !== "false",
    returnUrl: HOST,
    trialDays: parseInt(SUBSCRIPTION_FREE_TRIAL),
  };

  const confirmationUrl = await shopify
    .graphql(RECURRING_CREATE, variables)
    .then((response) => {
      return response.appSubscriptionCreate.confirmationUrl;
    });

  return confirmationUrl;
};
