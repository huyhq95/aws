const webhookTopics = Object.values(
  require("../../../app-api/webhooks/webhook-topics")
);

module.exports = {
  GET_WEBHOOK_SUBSCRIPTIONS: `
        query getWebhookSubscriptions {
            webhookSubscriptions(topics: [${webhookTopics.join(",")}], first: ${
    webhookTopics.length
  }) {
                edges {
                    node {
                        topic
                    }
                }
            }
        }
    `,
};
