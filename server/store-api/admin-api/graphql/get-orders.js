import { getShopifyApi } from "../../../utils/get-shopify-api";
import util from "util";
const wait = util.promisify(setTimeout);

const GET_ORDERS = `
    query {
        orders(first: 5, reverse:true) {
            edges {
              node {
                id
                name
              }
            }
        }
    }
`;

module.exports.getOrders = async (shop, accessToken) => {
  const shopify = getShopifyApi(shop, accessToken);
  let response;
  try {
    response = await shopify.graphql(GET_ORDERS);
  } catch (error) {
    if (error.extensions && error.extensions.code === "THROTTLED") {
      await wait(1000);
      await this.getOrders(shop, accessToken);
    }
    throw error;
  }
  if (response) {
    return response.orders.edges;
  } else {
    return null;
  }
};
