require("isomorphic-fetch");
const Shopify = require("shopify-api-node");
const { ApiVersion } = require("@shopify/koa-shopify-graphql-proxy");

const GET_ACTIVE_SUBSCRIPTIONS_QUERY = `
  query {
    app {
      installation {
        activeSubscriptions {
          id
          status
        }
      }
    }
  }
`;

module.exports = async (shop, accessToken) => {
  const shopify = new Shopify({
    shopName: shop,
    accessToken: accessToken,
    apiVersion: ApiVersion.April20,
  });
  const response = await shopify.graphql(GET_ACTIVE_SUBSCRIPTIONS_QUERY);
  if (
    response &&
    response.app &&
    response.app.installation &&
    response.app.activeSubscriptions
  ) {
    return response.app.activeSubscriptions;
  } else {
    return null;
  }
};
