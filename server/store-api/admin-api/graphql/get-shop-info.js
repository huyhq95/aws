require("isomorphic-fetch");
const Shopify = require("shopify-api-node");
const { ApiVersion } = require("@shopify/koa-shopify-graphql-proxy");

const GET_SHOP_INFO_QUERY = `
  query {
    shop {
      ianaTimezone
      plan {
        shopifyPlus
      }
    }
  }
`;

module.exports = async (shop, accessToken) => {
  const shopify = new Shopify({
    shopName: shop,
    accessToken: accessToken,
    apiVersion: ApiVersion.April20,
  });
  const response = await shopify.graphql(GET_SHOP_INFO_QUERY);
  if (response && response.shop) {
    return {
      timezone: response.shop.ianaTimezone,
      plan: response.shop.plan,
    };
  } else {
    return null;
  }
};
