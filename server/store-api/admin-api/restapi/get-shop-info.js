import { getShopifyApi } from "../../../utils/get-shopify-api";

module.exports.getShopInfo = async (shop, accessToken) => {
  const shopify = getShopifyApi(shop, accessToken);
  const response = await shopify.shop.get();
  if (response) {
    return response;
  } else {
    return null;
  }
};
