import { getShopifyApi } from "../../../utils/get-shopify-api";

module.exports.getOrderTransaction = async (shop, accessToken, orderId) => {
  const shopify = getShopifyApi(shop, accessToken);
  const response = await shopify.transaction.list(
    orderId.substring(orderId.lastIndexOf("/") + 1)
  );
  if (response) {
    return response;
  } else {
    return null;
  }
};
