import { getShopifyApi } from "../../../utils/get-shopify-api";
import { isEmpty } from "lodash";

module.exports.getActiveTheme = async (shop, accessToken) => {
  const shopify = getShopifyApi(shop, accessToken);
  let theme = await shopify.theme.list({ role: "main" });
  if (!isEmpty(theme) && !isEmpty(theme[0])) {
    return theme[0].id;
  } else {
    return null;
  }
};
