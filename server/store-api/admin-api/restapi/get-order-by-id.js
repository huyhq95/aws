import { getShopifyApi } from "../../../utils/get-shopify-api";

module.exports.getOrderById = async (shop, accessToken, orderId) => {
  const shopify = getShopifyApi(shop, accessToken);
  const response = await shopify.order.get(
    orderId.substring(orderId.lastIndexOf("/") + 1)
  );
  if (response) {
    return response;
  } else {
    return null;
  }
};
