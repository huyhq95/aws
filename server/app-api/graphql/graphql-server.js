const { merge } = require("lodash");
const { ApolloServer } = require("apollo-server-koa");
const ShopSource = require("../../datasources/shops-ds");
const shopSchema = require("./schemas/shop-schema");
const TemplatesSource = require("../../datasources/templates-ds");
const templatesSchema = require("./schemas/templates-schema");

// currently if we passs schema explicitly to the ApolloServer constructor, there will be an error with type Upload
// so we do not use makeExecutableSchema and mergeSchemas functions
// we need to merge typeDefs and resolvers manually and then parse them into ApolloServer constructor

const baseTypeDefs = `
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }
`;

const baseResolvers = {};

const graphqlServer = new ApolloServer({
  typeDefs: [baseTypeDefs, shopSchema.typeDefs, templatesSchema.typeDefs],
  resolvers: merge(
    baseResolvers,
    shopSchema.resolvers,
    templatesSchema.resolvers
  ),
  dataSources: () => ({
    shopDataSource: new ShopSource(),
    templatesDataSource: new TemplatesSource(),
  }),
  context: ({ ctx }) => ctx,
});

module.exports = graphqlServer;
