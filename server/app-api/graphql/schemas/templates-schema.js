import { gql } from "apollo-server-koa";
import { getOrders } from "../../../store-api/admin-api/graphql/get-orders";
import { getOrderById } from "../../../store-api/admin-api/restapi/get-order-by-id";
import { formatTemplateCode } from "../../../admin-processor/format-template-code";
import { getShopInfo } from "../../../store-api/admin-api/restapi/get-shop-info";
import { getOrderTransaction } from "../../../store-api/admin-api/restapi/get-order-transaction";
import { createTemplateReceiptLiquid } from "../../../admin-processor/create-template-receipt-liquid";
const dotenv = require("dotenv");
dotenv.config();
const { TEMPLATES_RECEIPT_LIQUID_NAME } = process.env;

const typeDefs = gql`
  input templateInput {
    templateName: String
    templateCode: String
    isDefault: Boolean
  }

  type Template {
    _id: String
    templateName: String
    templateCode: String
    isDefault: Boolean
    shopName: String
    lastModifiled: String
  }

  type templateMutationResponse {
    success: Boolean
    message: String
  }

  type GetTemplateRespone {
    message: String
    success: Boolean
    data: Template
  }

  type TemplateQueryResponse {
    message: String
    success: Boolean
    data: [Template]
  }

  type GetPreviewTemplateRespone {
    message: String
    success: Boolean
    data: previewTemplate
  }

  type previewTemplate {
    previewTemplateCode: String
  }

  type OrdersRespone {
    id: String
    name: String
  }

  type GetOrdersRespone {
    success: Boolean
    orders: [OrdersRespone]
  }

  extend type Query {
    getPreviewTemplate(
      templateCode: String
      orderId: String
    ): GetPreviewTemplateRespone
    getTemplate(templateId: String): GetTemplateRespone
    getTemplatesByShop: TemplateQueryResponse
    getOrders: GetOrdersRespone
  }

  extend type Mutation {
    saveTemplate(
      template: templateInput
      templateId: String
    ): templateMutationResponse
    updateTemplateDefault(
      templateId: String
      lastModifiled: String
    ): templateMutationResponse
  }
`;

const resolvers = {
  Query: {
    getTemplatesByShop: async (_, { __ }, { session, dataSources }) => {
      const { shop } = session;
      try {
        const response = await dataSources.templatesDataSource.getTemplatesByShop(
          shop
        );
        return {
          message: "Get templates by shop success",
          success: true,
          data: response,
        };
      } catch (err) {
        return {
          message: "Error to get templates by shop",
          success: false,
          data: null,
        };
      }
    },

    getTemplate: async (_, { templateId }, { dataSources }) => {
      try {
        let template = await dataSources.templatesDataSource.getTemplateById(
          templateId
        );

        return {
          message: "Get template success",
          success: true,
          data: template,
        };
      } catch (err) {
        return {
          message: "Get template failed",
          success: false,
          data: null,
        };
      }
    },
    getOrders: async (_, { __ }, { session }) => {
      try {
        const { shop, accessToken } = session;
        let orders = await getOrders(shop, accessToken);
        let listOrder = [];
        for (let index = 0; index < orders.length; index++) {
          listOrder.push(orders[index].node);
        }
        return {
          success: true,
          orders: listOrder,
        };
      } catch (err) {
        return {
          success: false,
          orders: null,
        };
      }
    },
    getPreviewTemplate: async (_, { templateCode, orderId }, { session }) => {
      try {
        const { shop, accessToken } = session;
        let shopInfo = await getShopInfo(shop, accessToken);
        let orderTransaction = await getOrderTransaction(
          shop,
          accessToken,
          orderId
        );
        let order = await getOrderById(shop, accessToken, orderId);
        let previewTemplateCode = await formatTemplateCode(
          {
            order,
            shop: shopInfo,
            orderTransaction,
          },
          templateCode
        );
        return {
          message: "Get template success",
          success: true,
          data: {
            previewTemplateCode,
          },
        };
      } catch (err) {
        return {
          message: "Get template failed",
          success: false,
          data: null,
        };
      }
    },
  },
  Mutation: {
    saveTemplate: async (
      _,
      { template, templateId },
      { session, dataSources }
    ) => {
      const { shop, accessToken } = session;
      try {
        let templateExist = await dataSources.templatesDataSource.getTemplate(
          template.templateName,
          templateId,
          shop
        );
        if (templateExist) {
          return {
            message: "Template.Error.nameTaken",
            success: false,
          };
        } else {
          if (template.isDefault) {
            let shopInfo = await dataSources.shopDataSource.getShopInfo(shop);
            await createTemplateReceiptLiquid(
              shop,
              accessToken,
              TEMPLATES_RECEIPT_LIQUID_NAME,
              shopInfo.activeTheme,
              template.templateCode
            );
            await dataSources.templatesDataSource.removeDefaultTemplate();
          }
          template.shopName = shop;
          await dataSources.templatesDataSource.saveTemplate(
            template,
            templateId
          );

          return {
            message: templateId
              ? "Template.Success.successfullyUpdateTemplate"
              : "Template.Success.successfullyCreatedTemplate",
            success: true,
          };
        }
      } catch (err) {
        console.log(
          "Error to save receipt template " +
            template.templateName +
            " for shop " +
            shop +
            "",
          err
        );
        return {
          message: "Template.Error.saveTemplateFail",
          success: false,
        };
      }
    },
    updateTemplateDefault: async (
      _,
      { templateId, lastModifiled },
      { session, dataSources }
    ) => {
      const { shop, accessToken } = session;
      let template = await dataSources.templatesDataSource.getTemplateById(
        templateId
      );

      try {
        await dataSources.templatesDataSource.updateTemplateDefault(
          shop,
          templateId,
          lastModifiled
        );
        let shopInfo = await dataSources.shopDataSource.getShopInfo(shop);
        await createTemplateReceiptLiquid(
          shop,
          accessToken,
          shopInfo.activeTheme,
          template.templateCode
        );
        return {
          message: "Update template default success",
          success: true,
        };
      } catch (error) {
        console.log(
          "Error to update default receipt template " +
            template.templateName +
            " for shop " +
            shop +
            "",
          error
        );
        return {
          message: "Error to update template default",
          success: false,
        };
      }
    },
  },
};

module.exports = { typeDefs, resolvers };
