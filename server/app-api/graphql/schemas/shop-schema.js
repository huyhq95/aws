const { gql } = require("apollo-server-koa");
const GraphQLJSON = require("graphql-type-json");

const {
  getSubscriptionUrl,
} = require("../../../store-api/admin-api/graphql/get-subscription-url");
const typeDefs = gql`
  scalar JSON

  type MutationResponse {
    success: Boolean
    message: String
  }

  extend type Query {
    shopInfo: JSON
    subscriptionUrl: String
  }

  extend type Mutation {
    setShopTourGuideStatus(status: Boolean): MutationResponse
    saveSettings(settings: JSON): MutationResponse
    setSubscriptionUpdated: MutationResponse
  }
`;

const resolvers = {
  JSON: GraphQLJSON,
  Query: {
    shopInfo: async (_, __, { dataSources, session }) => {
      const { shop } = session;
      try {
        let shopInfo = await dataSources.shopDataSource.getShopInfo(
          shop,
          false
        );
        if (shopInfo) {
          return {
            appSubscription: shopInfo.appSubscription,
            plan: shopInfo.plan,
            timezoneGMT: shopInfo.timezoneGMT,
            showTourGuide: shopInfo.showTourGuide,
            settings: shopInfo.settings ? shopInfo.settings : null,
            timezone: shopInfo.timezone,
          };
        }
      } catch (error) {
        console.error(error);
      }
      return null;
    },

    subscriptionUrl: async (_, __, { session }) => {
      try {
        const { shop, accessToken } = session;
        let subscriptionUrl = await getSubscriptionUrl(shop, accessToken);
        return subscriptionUrl;
      } catch (error) {
        console.error(error);
      }
      return null;
    },
  },

  Mutation: {
    saveSettings: async (_, { settings }, { dataSources, session }) => {
      try {
        await dataSources.shopDataSource.saveSettings(session, settings);
        return {
          success: true,
        };
      } catch (error) {
        console.error(error);
        return { success: false, message: error.message };
      }
    },

    setShopTourGuideStatus: async (_, { status }, { dataSources, session }) => {
      try {
        let response = await dataSources.shopDataSource.saveShopInfo(
          session.shop,
          { showTourGuide: status }
        );
        if (response) {
          return {
            success: true,
            message: "Tour guide status updated successfully",
          };
        }
      } catch (error) {
        return { success: false, message: error.message };
      }
    },

    setSubscriptionUpdated: async (_, __, { dataSources, session }) => {
      try {
        const shopInfo = await dataSources.shopDataSource.getShopInfo(
          session.shop
        );
        const response = await dataSources.shopDataSource.saveShopInfo(
          session.shop,
          {
            appSubscription: {
              ...shopInfo.appSubscription,
              subscriptionUpdated: false,
            },
          }
        );
        if (response) {
          return {
            success: true,
            message: "Subscription updated successfully",
          };
        }
      } catch (error) {
        return { success: false, message: error.toString() };
      }
    },
  },
};

module.exports = { typeDefs, resolvers };
