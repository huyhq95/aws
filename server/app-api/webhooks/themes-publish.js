const ShopDataSource = require("../../datasources/shops-ds");
const {
  getEasyReceiptLiquid,
} = require("../../admin-processor/get-easy-receipt-liquid");

const {
  createEasyReceiptLiquid,
} = require("../../admin-processor/create-easy-receipt-liquid");

module.exports = async function handleThemesPublish(ctx, webhook) {
  const shop = webhook.domain;
  const shopDataSource = new ShopDataSource();
  shopDataSource.initialize({ context: ctx });

  const {
    accessToken,
    activeTheme,
    appSubscription,
  } = await shopDataSource.getShopInfo(shop, true);
  if (
    activeTheme !== webhook.payload.id &&
    appSubscription &&
    appSubscription.status === "ACTIVE"
  ) {
    try {
      await getEasyReceiptLiquid(shop, accessToken, webhook.payload.id);
    } catch (error) {
      const {
        easyReceiptVariables,
        templateReceiptLiquid,
        easyReceiptControls,
      } = await getEasyReceiptLiquid(shop, accessToken, activeTheme);
      await createEasyReceiptLiquid(
        shop,
        accessToken,
        webhook.payload.id,
        easyReceiptVariables,
        templateReceiptLiquid,
        easyReceiptControls
      );
    }
    await shopDataSource.updateActiveThemeByShop(shop, webhook.payload.id);
  }
};
