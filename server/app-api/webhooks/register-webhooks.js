const {
  registerWebhook: registerShopifyWebhook,
} = require("@shopify/koa-shopify-webhooks");
const { ApiVersion } = require("@shopify/koa-shopify-graphql-proxy");
const Shopify = require("shopify-api-node");

const {
  APP_UNINSTALLED,
  APP_SUBSCRIPTIONS_UPDATE,
  SHOP_UPDATE,
  THEMES_PUBLISH,
} = require("./webhook-topics");
const {
  GET_WEBHOOK_SUBSCRIPTIONS,
} = require("../../store-api/admin-api/graphql/get-webhook-subscriptions");

const registerWebhook = async (shop, accessToken, type, url, apiVersion) => {
  const registration = await registerShopifyWebhook({
    address: `${process.env.HOST}${url}`,
    topic: type,
    accessToken,
    shop,
    apiVersion,
  });

  if (!registration.success) {
    console.error(
      `Error to register webhook ${type}`,
      JSON.stringify(registration, null, 2)
    );
  }
  return registration.success;
};

const registerWebhooks = async (shop, accessToken) => {
  const shopify = new Shopify({ shopName: shop, accessToken });
  const webhookSubscriptionsResponse = await shopify.graphql(
    GET_WEBHOOK_SUBSCRIPTIONS
  );

  const webhookSubscriptions =
    webhookSubscriptionsResponse.webhookSubscriptions.edges;
  const existingWebhookTopics = [];

  webhookSubscriptions.map((webhook) => {
    existingWebhookTopics.push(webhook.node.topic);
  });

  const webhooks = [];

  if (!existingWebhookTopics.includes(APP_SUBSCRIPTIONS_UPDATE)) {
    webhooks.push(
      registerWebhook(
        shop,
        accessToken,
        APP_SUBSCRIPTIONS_UPDATE,
        "/webhooks/app-subscription-updated",
        ApiVersion.April20
      )
    );
  }

  if (!existingWebhookTopics.includes(SHOP_UPDATE)) {
    webhooks.push(
      registerWebhook(
        shop,
        accessToken,
        SHOP_UPDATE,
        "/webhooks/shop-updated",
        ApiVersion.April20
      )
    );
  }
  if (!existingWebhookTopics.includes(APP_UNINSTALLED)) {
    webhooks.push(
      registerWebhook(
        shop,
        accessToken,
        APP_UNINSTALLED,
        "/webhooks/app-uninstalled",
        ApiVersion.April20
      )
    );
  }
  if (!existingWebhookTopics.includes(THEMES_PUBLISH)) {
    webhooks.push(
      registerWebhook(
        shop,
        accessToken,
        THEMES_PUBLISH,
        "/webhooks/themes-publish",
        ApiVersion.April20
      )
    );
  }

  if (webhooks.length > 0) {
    Promise.all(webhooks).catch((error) => {
      console.error(error);
    });
  }
};

exports.registerWebhooks = registerWebhooks;
