const ShopDataSource = require("../../datasources/shops-ds");
const TemplateDataSource = require("../../datasources/templates-ds");

module.exports = async function handleAppUninstalled(ctx, webhook) {
  const shop = webhook.domain;

  const shopDataSource = new ShopDataSource();
  shopDataSource.initialize({ context: ctx });
  const templateDataSource = new TemplateDataSource();
  templateDataSource.initialize({ context: ctx });

  await templateDataSource.deleteTemplatesByShop(shop);
  await shopDataSource.saveShopInfo(shop, {
    appSubscription: null,
  });
};
