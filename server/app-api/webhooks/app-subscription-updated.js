const ShopDataSource = require("../../datasources/shops-ds");
const TemplatesDataSource = require("../../datasources/templates-ds");
const {
  getShopInfo,
} = require("../../store-api/admin-api/restapi/get-shop-info");
const { getShopifyApi } = require("../../utils/get-shopify-api");
require("dotenv").config();
const fs = require("fs");
const { dirname } = require("path");
const { DIR_TEMPLATES_DEFAULT, DIR_LIQUID_DEFAULT } = process.env;

const {
  createTemplateReceiptLiquid,
} = require("../../admin-processor/create-template-receipt-liquid");
const { updateLocales } = require("../../admin-processor/update-locales");

module.exports = async function (ctx, webhook) {
  const shop = webhook.domain;
  const appSubscription = webhook.payload;

  const shopDataSource = new ShopDataSource();
  const templatesDataSource = new TemplatesDataSource();
  shopDataSource.initialize({ context: ctx });
  templatesDataSource.initialize({ context: ctx });

  let shopInfo = {
    shop,
    shopId: appSubscription.app_subscription.admin_graphql_api_shop_id,
    appSubscription: {
      status: appSubscription.app_subscription.status,
      subscriptionId: appSubscription.app_subscription.admin_graphql_api_id,
      subscriptionUpdated: true,
    },
  };

  const { accessToken, activeTheme } = await shopDataSource.getShopInfo(
    shop,
    true
  );

  const response = await getShopInfo(shop, accessToken);
  let locale;

  if (response) {
    shopInfo.timezone = response.iana_timezone;
    shopInfo.locale = response.primary_locale;
    locale = response.primary_locale;
  }
  await shopDataSource.saveShopInfo(shop, shopInfo);

  let dirTemplateDefault =
    locale == "ja"
      ? `${DIR_TEMPLATES_DEFAULT}/ja`
      : `${DIR_TEMPLATES_DEFAULT}/en`;

  fs.readdir(dirTemplateDefault, async (err, files) => {
    if (err) {
      throw err;
    }

    for (let i = 0; i < files.length; i++) {
      await templatesDataSource.saveTemplate({
        templateName: files[i].substring(0, files[i].indexOf(".")),
        templateCode: fs.readFileSync(
          `${dirTemplateDefault}/${files[i]}`,
          "utf8"
        ),
        isDefault: false,
        shopName: shop,
      });
    }
  });

  fs.readdir(DIR_LIQUID_DEFAULT, async (err, files) => {
    if (err) {
      throw err;
    }

    for (let i = 0; i < files.length; i++) {
      await createTemplateReceiptLiquid(
        shop,
        accessToken,
        `snippets/${files[i]}`,
        activeTheme,
        fs.readFileSync(`${DIR_LIQUID_DEFAULT}/${files[i]}`, "utf8")
      );
    }
  });

  await updateLocales(shop, accessToken, locale, activeTheme);

  const shopifyApi = await getShopifyApi(shop, accessToken);

  if (appSubscription.app_subscription.status !== "ACTIVE") {
    await shopifyApi.asset.update(activeTheme, {
      key: process.env.TEMPLATES_RECEIPT_LIQUID_NAME,
      value: "",
    });

    await shopifyApi.asset.update(activeTheme, {
      key: process.env.EASY_RECEIPT_VARIABLES_LIQUID,
      value: "",
    });
  } else {
    const templateDefault = await templatesDataSource.getTemplateDefaultByShop(
      shop
    );

    const easyReceiptVariable = fs.readFileSync(
      `${dirname(
        dirname(dirname(__dirname))
      )}/liquid/default/easy-receipt-variables.liquid`,
      "utf-8"
    );

    await shopifyApi.asset.update(activeTheme, {
      key: process.env.EASY_RECEIPT_VARIABLES_LIQUID,
      value: easyReceiptVariable,
    });

    await shopifyApi.asset.update(activeTheme, {
      key: process.env.TEMPLATES_RECEIPT_LIQUID_NAME,
      value: templateDefault && templateDefault.templateCode,
    });
  }
};
