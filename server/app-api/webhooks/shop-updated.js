const ShopDataSource = require("../../datasources/shops-ds");
const { updateLocales } = require("../../admin-processor/update-locales");

module.exports = async function (ctx, webhook) {
  const shop = webhook.domain;
  const shopInfo = webhook.payload;
  const shopDataSource = new ShopDataSource();
  shopDataSource.initialize({ context: ctx });
  const shopInfoFromDB = await shopDataSource.getShopInfo(shop, true);

  if (shopInfoFromDB.timezone !== shopInfo.iana_timezone) {
    shopInfoFromDB.timezone = shopInfo.iana_timezone;
  }

  if (shopInfoFromDB.locale !== shopInfo.primary_locale) {
    shopInfoFromDB.locale = shopInfo.primary_locale;
    await updateLocales(
      shop,
      shopInfoFromDB.accessToken,
      shopInfo.primary_locale,
      shopInfoFromDB.activeTheme
    );
  }

  await shopDataSource.saveShopInfo(shop, shopInfoFromDB);
};
