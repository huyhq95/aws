import Shopify from "shopify-api-node";
import { ApiVersion } from "@shopify/koa-shopify-graphql-proxy";
let shopifyApi = new Map();

module.exports.getShopifyApi = (shop, accessToken, isRefresh) => {
  if (shopifyApi.has(shop) && !isRefresh) {
    return shopifyApi.get(shop).shopifyInstance;
  }

  const shopify = new Shopify({
    shopName: shop,
    accessToken,
    autoLimit: true,
    apiVersion: ApiVersion.April20,
  });
  shopifyApi.set(shop, {
    shopifyInstance: shopify,
  });

  return shopify;
};
