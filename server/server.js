require("@babel/polyfill");
const dotenv = require("dotenv");
require("isomorphic-fetch");
const {
  default: createShopifyAuth,
  verifyRequest,
} = require("@shopify/koa-shopify-auth");
const { registerWebhooks } = require("./app-api/webhooks/register-webhooks");
const Koa = require("koa");
const nextjs = require("next");
const Router = require("@koa/router");
const session = require("koa-session");
const mongo = require("koa-mongo");
const mount = require("koa-mount");
const webhookRouter = require("./routes/webhook-routes");
const appApiRouter = require("./routes/app-api-routes");
const serveStatic = require("koa-static");
const path = require("path");
const {
  getSubscriptionUrl,
} = require("./store-api/admin-api/graphql/get-subscription-url");
const ShopDataSource = require("./datasources/shops-ds");
dotenv.config();
const port = parseInt(process.env.PORT, 10) || 3000;
const isDevEnv = process.env.NODE_ENV === "development";
const nextApp = nextjs({
  dev: isDevEnv,
});
const handleNextRequest = nextApp.getRequestHandler();
const { SHOPIFY_API_SECRET, SHOPIFY_API_KEY, SCOPES } = process.env;
const {
  MONGODB_URI,
  MONGODB_POOL_MAX,
  MONGODB_POOL_MIN,
  APP_NAME,
  BACKUP_DIR = "backup",
} = process.env;
const {
  getActiveTheme,
} = require("./store-api/admin-api/restapi/get-active-theme");

nextApp.prepare().then(() => {
  const koaApp = new Koa();
  const router = new Router();
  console.log('aaâ')
  koaApp.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      if (401 == err.status) {
        ctx.status = 401;
        ctx.set("WWW-Authenticate", "Basic");
        ctx.body = "cant haz that";
      } else {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        ctx.app.emit("error", err, ctx);
      }
    }
  });
  koaApp.on("error", (err, ctx) => {
    console.error(err);
  });

  koaApp.use(
    mount(
      `/${BACKUP_DIR}`,
      serveStatic(`${path.dirname(__dirname)}/public/${BACKUP_DIR}`)
    )
  );

  koaApp.use(
    mongo({
      uri: MONGODB_URI,
      max: parseInt(MONGODB_POOL_MAX, 10) || 100,
      min: parseInt(MONGODB_POOL_MIN, 10) || 1,
      acquireTimeoutMillis: 20000,
    })
  );

  koaApp.keys = [SHOPIFY_API_SECRET];
  koaApp.use(
    session(
      {
        key: "_sess_",
        sameSite: "none",
        secure: true,
      },
      koaApp
    )
  );

  koaApp.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET,
      scopes: SCOPES.split(","),
      accessMode: "offline",
      async afterAuth(ctx) {
        await handleAfterAuth(ctx);
      },
    })
  );

  router.get("(.*)", verifyRequest(), async (ctx) => {
    const { locale, shop } = ctx.query;
    const cookieOptions = {
      httpOnly: false,
      secure: true,
      sameSite: "none",
    };
    if (locale) {
      ctx.cookies.set("locale", locale, cookieOptions);
    }
    if (shop) {
      ctx.cookies.set("shopOrigin", shop, cookieOptions);
    }
    await handleNextRequest(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  koaApp.use(appApiRouter.allowedMethods());
  koaApp.use(appApiRouter.routes());
  koaApp.use(webhookRouter.allowedMethods());
  koaApp.use(webhookRouter.routes());
  koaApp.use(router.allowedMethods());
  koaApp.use(router.routes());

  koaApp.listen(port, () => {
    console.info(
      `${APP_NAME ? APP_NAME : "Server"} is ready on http://localhost:${port}`
    );
  });
});

async function handleAfterAuth(ctx) {
  const { shop, accessToken } = ctx.session;

  ctx.cookies.set("shopOrigin", shop, {
    httpOnly: false,
    secure: true,
    sameSite: "none",
  });

  const shopDataSource = new ShopDataSource();
  shopDataSource.initialize({ context: ctx });
  const shopInfo = await shopDataSource.getShopInfo(shop, true);

  if (shopInfo && shopInfo.accessToken === accessToken) {
    ctx.redirect("/");
  } else {
    let activeTheme = await getActiveTheme(shop, accessToken);
    await shopDataSource.saveShopInfo(shop, {
      shop,
      accessToken,
      showTourGuide: true,
      appSubscription: {
        subscriptionUpdated: false,
      },
      activeTheme,
    });
    await registerWebhooks(shop, accessToken);

    const subscriptionUrl = await getSubscriptionUrl(shop, accessToken);
    ctx.redirect(subscriptionUrl);
  }
}
